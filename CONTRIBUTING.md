# Contribution Guide

## Table of Contents

* [Overview](#overview)
* [Adding a brand new feature or enhancement](#adding-a-brand-new-feature-or-enhancement)
* [Reporting a bug](#reporting-a-bug)
* [Fixing a bug](#fixing-a-bug)
* [Proposing enhancements](#proposing-enhancements)

## Overview

This guide will lead you through common guidelines that we follow  when contributing.

## Adding a brand new feature or enhancement

If you have a *Developer* or *Maintainer* role then follow instructions below.
Otherwise, the only option that is available for you is to [propose an enhancement](#proposing-enhancements).

At the very beginning make sure that the corresponding issue is not being
developed already (i.e. it is not in *Doing*, *Code Review* or *Closed* [issue lists](https://gitlab.com/Medelyan/crypt/boards)).
If it's OK, then follow guidelines below.

###### *1. Firstly, create a new branch for your feature.*

Move corresponding issue to the *Doing* issue list and assign it to yourself
so that other contributors may know that someone is already working on that issue.

Then you are ready to start working. Create a separate branch for a feature.
The naming convention is:
```
feature/<feature-short-description>
```
For instance:
```
feature/add-contribution-guide
```

###### *2. Follow TDD principles.*

While developing, please, follow the *Test Driven Development* guidelines, the main of which
sounds like *"Tests first!"*.

###### *3. Make required changes, push the branch and create a merge request.*

Assign your MR to one of the *Maintainers* (even if you are a *Maintainer* yourself)
and let them take a look at your code and finally merge it.

Do not forget to move a corresponding issue to the *Code Review* issue list. 

## Reporting a bug

Add a corresponding issue to *Bug* issue list (or simply add the *Bug* label to your new issue).  
In description, write a detailed information about what went wrong.

## Fixing a bug

If you have a *Developer* or *Maintainer* role you are able to fix a reported bug.  

As for the [adding a brand new feature or enhancement](#adding-a-brand-new-feature-or-enhancement),
check if the corresponding issue is in one of *Doing*, *Code Review* or *Closed*
[issue lists](https://gitlab.com/Medelyan/crypt/boards). If it is not then you are ready to go.

Move the issue to the *Doing* issue list and assign yourself to it.

A fix for the bug has to be in separate branch named like the following:
```
bugfix/<bug-name-or-short-description>
```
For instance:
```
bugfix/zero-to-power-of-zero-gives-zero
```

After you've finished, assign your MR to one of the *Maintainers* (even if you are a 
*Maintainer* yourself) and let them take a look at your code and finally merge it.

Do not forget to move a corresponding issue to the *Code Review* issue list.

## Proposing enhancements

Add a corresponding issue to *Enhancement* issue list (or simply add the *Enhancement* label to your new issue).  
In description, write detailed information about what can be enhanced or added.
