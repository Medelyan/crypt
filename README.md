[![](https://jitpack.io/v/com.gitlab.Medelyan/crypt.svg)](https://jitpack.io/#com.gitlab.Medelyan/crypt)
[&#91;Russian README&#93;](https://gitlab.com/Medelyan/crypt/blob/master/README_RU.md)
[&#91;Examples&#93;](https://gitlab.com/Medelyan/crypt/blob/master/EXAMPLES_RU.md)
[&#91;Changelog&#93;](https://gitlab.com/Medelyan/crypt/blob/master/CHANGELOG.md)
# Crypt
## Table of contents

* [Overview](#overview)
* [Usage](#usage)
    * [Add dependency](#add-dependency)
    * [Use it](#use-it)
* [Contribution](#contribution)

## Overview

This project contains useful functions and features for encrypting/decrypting
text. It has been created due to studying reasons and not for actual usage.

## Usage

### Add dependency

Go to [JitPack corresponding page of the project](https://jitpack.io/#com.gitlab.Medelyan/crypt). There you can choose
the version you need or simply the latest one and include it as a Maven/Gradle/etc dependency.

> Notice: if you want to have javadoc as well as the lib itself, you can, for instance, ask your IDEA to do that for you.
If you are using Intellij IDEA, right-click your project root folder and choose `Maven -> Download documentation` (or 
similar for Gradle and others).
FIXME: SM: Doesn't work for me :(

### Use it

Firsly, you need to *build a crypt*:
```java
// create an instance
Crypt crypt = Crypt.build();
```

After that you can use it:
```java
// use what you need; for example:
// for gamma encryption/decryption you need to set alphabet first
// (alphabet is preserved, so you have to set it once)
crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
int[] gamma = new int[]{30, 14, 4, 20, 4};
String decrypted = crypt.decrypt("сымцйоямжйр", gamma);
decrypted.equals("университет"); // -> true
```

Make sure to check our [Examples](https://gitlab.com/Medelyan/crypt/blob/master/EXAMPLES_RU.md) guide to see how you can solve tasks utilizing Crypt!

## Contribution

Take a look at [contribution guide](https://gitlab.com/Medelyan/crypt/blob/master/CONTRIBUTING.md)