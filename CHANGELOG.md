# Changelog

## 0.4.0

Implemented features:
* `extractGamma()` - extracts gamma offsets array from given number;
* `egEncrypt()` - encrypts a message according to ElGamal encryption;
* `egDecrypt()` - decrypts a message according to ElGamal encryption;
* `egSign()` - creates signature for a message (based on ElGamal encryption);
* `egCheckSignature()` - checks if given signate is valid for given message
(according to ElGamal encryption);
* `rsaEncrypt()` - encrypts message according to RSA encryption algorithm;
* `rsaDecrypt()` - decrypts message according to RSA algorithm;
* `rsaCommonModuleAttack()` - lets to find secret message on the base of common module;
* `schnorrSign()` - creates signature for a message according to Schnorr algorythm;
* `schnorrCheckSignature()` - checks that signature belongs to the right person according to Schnorr algorithm.

## 0.3.1

Miscellaneous:
* ENGLISH_LOWERCASE - new alphabet constant is available in Alphabets class.
* EncodingCharStrategy - the interface to be implemented for char encoding/decoding. 
* CharToBinaryNumberEncoder - implementation of EncodingStrategy interface.

## 0.3.0

Implemented features:
* `mhEncrypt()` - encryption of text with Merkle-Hellman algorithm;
* `mhDecrypt()` - decryption of encrypted with Merkle-Hellman algorithm text.

Miscellaneous:
* `mhPublicKey()` - calculation of public key by private key for Merkle-Hellman crypto-system.

## 0.2.0

Notable changes:
* migrated from `long` to `BigInteger`.

Implemented features:
* `euler()` - calculates number of integers up to the given one that are
relatively prime to it;
* `eqSolve()` - solves an equation `A * x = B (module M)` where A is prime
to M.

## 0.1.1

Implemented features:
* `dhDecrypt()` - decryption of encrypted with Diffie-Hellman algorithm text.

Miscellaneous:
* `dhKey()` - an easy way to get a secret key only in DH algorithm.

## 0.1.0

Implemented features:
* `encrypt(), decrypt()` - text encryption/decryption with given gamma;
* `remPow()` - calculation of the remainder of an `(q ^ a) % m` expression;
* `dhEncrypt(), dhBruteforce()` encryption & brute-forcing of text by Diffie-Hellman algorithm;
* `primitivesBy()` - finding all primitive elements of field by given field module;
* `isPrimitive()` - check if an element is primitive in given group.

Miscellaneous:
* `gamma()` - shortcut for constructing gamma offsets array;
* `setAlphabet()` - sets alphabets;
* `orderOf()` - order of an element of a group;
* `eqPower()` - finds the least possible power in equation.

Notes for devs:
* `build()` - added support for validation API. 