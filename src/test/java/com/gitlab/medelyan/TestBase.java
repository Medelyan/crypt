package com.gitlab.medelyan;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.math.BigInteger;

@SuppressWarnings("WeakerAccess")
public class TestBase {

    protected Crypt crypt;

    @Rule
    public ExpectedException expected = ExpectedException.none();

    @Before
    public void buildCrypt() {
        crypt = Crypt.build();
    }

    protected BigInteger anyValid() {
        return BigInteger.TEN;
    }

    protected String anyValidText() {
        return "text";
    }

    protected BigInteger anyValidBase() {
        return BigInteger.TEN;
    }

    protected BigInteger anyValidModule() {
        return BigInteger.TEN;
    }

    protected BigInteger anyValidPower() {
        return BigInteger.TEN;
    }

    protected int[] anyValidGamma() {
        return crypt.gamma(1, 2, 3);
    }

    protected BigInteger validMHModule() {
        return BigInteger.valueOf(80);
    }

    protected BigInteger validMHCoefficient() {
        return BigInteger.valueOf(29);
    }

    protected BigInteger[] validMHSequence() {
        return new BigInteger[] {
                BigInteger.valueOf(1),
                BigInteger.valueOf(2),
                BigInteger.valueOf(4),
                BigInteger.valueOf(9),
                BigInteger.valueOf(18),
                BigInteger.valueOf(35),
        };
    }

    protected MHPrivateKey validMHPrivateKey() {
        return new MHPrivateKey(validMHSequence(), validMHModule(), validMHCoefficient());
    }

    protected BigInteger[] validMHPublicKey() {
        return new BigInteger[] {
                BigInteger.valueOf(29),
                BigInteger.valueOf(58),
                BigInteger.valueOf(36),
                BigInteger.valueOf(21),
                BigInteger.valueOf(42),
                BigInteger.valueOf(55),
        };
    }
}
