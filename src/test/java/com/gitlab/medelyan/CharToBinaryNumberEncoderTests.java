package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.UnknownCharException;
import org.junit.Test;

import static org.junit.Assert.*;

public class CharToBinaryNumberEncoderTests extends TestBase {
    @Test
    public void testEncode_allValid() {
        EncodingCharStrategy encoder = new CharToBinaryNumberEncoder();

        String actual = encoder.encode('r', Alphabets.ENGLISH_LOWERCASE);

        String expected = "10010";
        assertEquals(actual, expected);
    }

    @Test
    public void testEncode_charIsNotFromAlphabet() {
        expected.expect(UnknownCharException.class);
        expected.expectMessage("Char is not in the alphabet.");
        EncodingCharStrategy encoder = new CharToBinaryNumberEncoder();

        encoder.encode('я', Alphabets.ENGLISH_LOWERCASE);
    }

    @Test
    public void testDecode_allValid() {
        EncodingCharStrategy encoder = new CharToBinaryNumberEncoder();

        char actual = encoder.decode("10010", Alphabets.ENGLISH_LOWERCASE);

        char expected = 'r';
        assertEquals(actual, expected);
    }

    @Test
    public void testDecode_binNumberOutOfAlphabetBounds() throws StringIndexOutOfBoundsException {
        expected.expect(StringIndexOutOfBoundsException.class);
        EncodingCharStrategy encoder = new CharToBinaryNumberEncoder();

        encoder.decode("1111111011110111", Alphabets.ENGLISH_LOWERCASE);
    }

}
