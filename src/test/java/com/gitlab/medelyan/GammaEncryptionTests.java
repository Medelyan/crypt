package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.AlphabetIsNotInitializedException;
import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import com.gitlab.medelyan.exceptions.UnknownCharException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GammaEncryptionTests extends TestBase {

    @Test
    public void test_text_isNull() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        String text = null;

        crypt.encrypt(text, anyValidGamma());
    }

    @Test
    public void test_gamma_isNull() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        int[] gamma = null;

        crypt.encrypt(anyValidText(), gamma);
    }

    @Test
    public void test_text_isEmpty() {
        String text = "";
        String actual = crypt.encrypt(text, anyValidGamma());


        String expected = "";
        assertEquals(expected, actual);
    }

    @Test
    public void test_gamma_isEmpty() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.EMPTY_GAMMA);
        int[] gamma = {};

        crypt.encrypt(anyValidText(), gamma);
    }

    @Test
    public void test_text_isValid_alphabetIsNotInitialized() {
        expected.expect(AlphabetIsNotInitializedException.class);
        crypt.encrypt(anyValidText(), anyValidGamma());
    }

    @Test
    public void test_text_containsSymbolsNotFromAlphabet() {
        expected.expect(UnknownCharException.class);
        String alphabet = "#?@";
        crypt.setAlphabet(alphabet);

        crypt.encrypt(anyValidText(), anyValidGamma());
    }

    @Test
    public void test_text_isValid_gamma_isValid() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        String text = "университет";
        int[] gamma = crypt.gamma(30, 14, 4, 20, 4);

        String actual = crypt.encrypt(text, gamma);

        String expected = "сымцйоямжйр";
        assertEquals(expected, actual);
    }

    @Test
    public void test_text_isValid_gamma_containsNumbersLargerThanAlphabetLength() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        int[] singleGamma = crypt.gamma(1 + Alphabets.RUSSIAN_LOWERCASE.length() * 10);
        String encrypted = "абвг";

        String actual = crypt.encrypt(encrypted, singleGamma);

        String expected = "бвгд";
        assertEquals(expected, actual);
    }
}
