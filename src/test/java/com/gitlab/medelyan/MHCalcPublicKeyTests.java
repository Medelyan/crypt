package com.gitlab.medelyan;

import org.junit.Test;

import java.math.BigInteger;
import static org.junit.Assert.*;

public class MHCalcPublicKeyTests extends TestBase {

    @Test
    public void test_All_isValid() {
        BigInteger[] expected = new BigInteger[] {
          BigInteger.valueOf(29),
          BigInteger.valueOf(58),
          BigInteger.valueOf(36),
          BigInteger.valueOf(21),
          BigInteger.valueOf(42),
          BigInteger.valueOf(55),
        };

        BigInteger[] actual = crypt.mhPublicKey(validMHPrivateKey());
        assertArrayEquals(expected, actual);
    }

}
