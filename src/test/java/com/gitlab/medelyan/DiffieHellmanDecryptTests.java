package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.AlphabetIsNotInitializedException;
import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class DiffieHellmanDecryptTests extends TestBase {

    @Test
    public void test_encrypted_isNull_othersValid() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        String encrypted = null;

        crypt.dhDecrypt(encrypted, anyValidBase(), anyValidPower(), anyValidPower(), anyValidModule());
    }

    @Test
    public void test_encrypted_isNull_key_isValid() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        String encrypted = null;

        crypt.dhDecrypt(encrypted, anyValidPower());
    }

    @Test
    public void test_base_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger base = BigInteger.ONE;

        crypt.dhDecrypt(anyValidText(), base, anyValidPower(), anyValidPower(), anyValidModule());
    }

    @Test
    public void test_alpha_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger alpha = BigInteger.ZERO;

        crypt.dhDecrypt(anyValidText(), anyValidBase(), alpha, anyValidPower(), anyValidModule());
    }

    @Test
    public void test_beta_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger beta = BigInteger.ZERO;

        crypt.dhDecrypt(anyValidText(), anyValidBase(), anyValidPower(), beta, anyValidModule());
    }

    @Test
    public void test_module_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger module = BigInteger.ONE;

        crypt.dhDecrypt(anyValidText(), anyValidBase(), anyValidPower(), anyValidPower(), module);
    }

    @Test
    public void test_key_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger key = BigInteger.ZERO;

        crypt.dhDecrypt(anyValidText(), key);
    }

    @Test
    public void test_all_areValid_alphabetIsNotInitialized() {
        expected.expect(AlphabetIsNotInitializedException.class);

        crypt.dhDecrypt(anyValidText(), anyValidBase(), anyValidPower(), anyValidPower(), anyValidModule());
    }

    @Test
    public void test_encrypted_and_key_areValid_alphabetIsNotInitialized() {
        expected.expect(AlphabetIsNotInitializedException.class);

        crypt.dhDecrypt(anyValidText(), anyValidPower());
    }

    @Test
    public void test_encrypted_isEmpty() {
        String encrypted = "";

        String actual = crypt.dhDecrypt(encrypted, anyValidBase(), anyValidPower(), anyValidPower(), anyValidModule());

        String expected = "";
        assertEquals(expected, actual);

        actual = crypt.dhDecrypt(encrypted, anyValidPower());
        assertEquals(expected, actual);
    }

    @Test
    public void test_encrypted_isValid() {
        String encrypted = "всупнхчхуауспз";
        BigInteger base = new BigInteger("10109");
        BigInteger module = new BigInteger("41903");
        BigInteger alpha = new BigInteger("21769");
        BigInteger beta = new BigInteger("17809");
        BigInteger key = new BigInteger("19570");
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        String actual = crypt.dhDecrypt(encrypted, base, alpha, beta, module);

        String expected = "биоинформатика";
        assertEquals(expected, actual);

        actual = crypt.dhDecrypt(encrypted, key);

        assertEquals(expected, actual);
    }
}
