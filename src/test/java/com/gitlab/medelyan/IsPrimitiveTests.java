package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import com.gitlab.medelyan.exceptions.GroupElementIsTooBigException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IsPrimitiveTests extends TestBase {

    @Test
    public void test_groupElementIsLessThan_0() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);

        BigInteger groupElement = new BigInteger("-1");
        crypt.isPrimitive(groupElement, anyValidModule());
    }

    @Test
    public void test_moduleIsLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);

        BigInteger module = BigInteger.ONE;
        crypt.isPrimitive(anyValid(), module);
    }

    @Test
    public void test_groupElementIsNotLessThanModule() {
        expected.expect(GroupElementIsTooBigException.class);
        BigInteger groupElement = new BigInteger("11");
        BigInteger module = new BigInteger("10");

        crypt.isPrimitive(groupElement, module);
    }

    @Test
    public void test_groupElement_is_0or1_module_isValid() {
        BigInteger groupElement = BigInteger.ZERO;

        boolean actual = crypt.isPrimitive(groupElement, anyValidModule());

        assertFalse(actual);
    }

    @Test
    public void test_groupElement_isNotPrimitive_module_isValid() {
        BigInteger groupElement = new BigInteger("5");
        BigInteger module = new BigInteger("41");

        boolean actual = crypt.isPrimitive(groupElement, module);

        assertFalse(actual);
    }

    @Test
    public void test_groupElement_isPrimitive_module_isValid() {
        BigInteger groupElement = new BigInteger("6");
        BigInteger module = new BigInteger("41");

        boolean actual = crypt.isPrimitive(groupElement, module);

        assertTrue(actual);
    }
}
