package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.AlphabetIsNotInitializedException;
import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class DiffieHellmanEncryptTests extends TestBase {

    @Test
    public void test_textIsEmpty() {
        String text = "";
        String actual = crypt.dhEncrypt(text, anyValidBase(), anyValidModule(), anyValidPower(), anyValidPower());

        String expected = "";
        assertEquals(expected, actual);
    }

    @Test
    public void test_textIsNull() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        String text = null;

        crypt.dhEncrypt(text, anyValidBase(), anyValidModule(), anyValidPower(), anyValidPower());
    }

    @Test
    public void test_base_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.NOT_SUPPORTED);
        BigInteger base = BigInteger.ZERO;

        crypt.dhEncrypt(anyValidText(), base, anyValidModule(), anyValidPower(), anyValidPower());
    }

    @Test
    public void test_module_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger module = BigInteger.ONE;

        crypt.dhEncrypt(anyValidText(), anyValidBase(), module, anyValidPower(), anyValidPower());
    }

    @Test
    public void test_alpha_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger alpha = BigInteger.ZERO;

        crypt.dhEncrypt(anyValidText(), anyValidBase(), anyValidModule(), alpha, anyValidPower());
    }

    @Test
    public void test_beta_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger beta = BigInteger.ZERO;

        crypt.dhEncrypt(anyValidText(), anyValidBase(), anyValidModule(), anyValidPower(), beta);
    }

    @Test
    public void test_allValid_alphabetIsNotInitialized() {
        expected.expect(AlphabetIsNotInitializedException.class);

        BigInteger base = new BigInteger("10109");
        BigInteger module = new BigInteger("41903");
        BigInteger alpha = new BigInteger("21769");
        BigInteger beta = new BigInteger("17809");

        String text = "биоинформатика";

        crypt.dhEncrypt(text, base, module, alpha, beta);
    }

    @Test
    public void test_allValid_fromLecture() {
        BigInteger base = new BigInteger("10109");
        BigInteger module = new BigInteger("41903");
        BigInteger alpha = new BigInteger("21769");
        BigInteger beta = new BigInteger("17809");

        String text = "биоинформатика";

        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        String actual = crypt.dhEncrypt(text, base, module, alpha, beta);

        String expected = "всупнхчхуауспз";
        assertEquals(expected, actual);
    }
}
