package com.gitlab.medelyan;

import org.junit.Test;

import java.math.BigInteger;

import static java.math.BigInteger.valueOf;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ElGamalCheckSignatureTests extends TestBase {

    @Test
    public void test_allValid_fromLecture() {
        BigInteger module = valueOf(337);
        BigInteger g = valueOf(15);
        BigInteger y = valueOf(303);

        BigInteger message = valueOf(309);
        Pair<BigInteger> signature = pairOf(31, 232);
        boolean signatureIsValid = crypt.egCheckSignature(message, signature, module, g, y);
        assertTrue(signatureIsValid);

        message = valueOf(19);
        signature = pairOf(31, 74);
        signatureIsValid = crypt.egCheckSignature(message, signature, module, g, y);
        assertTrue(signatureIsValid);

        message = valueOf(106);
        signature = pairOf(31, 324);
        signatureIsValid = crypt.egCheckSignature(message, signature, module, g, y);
        assertFalse(signatureIsValid);

        signature = pairOf(185, 81);
        signatureIsValid = crypt.egCheckSignature(message, signature, module, g, y);
        assertTrue(signatureIsValid);

        message = valueOf(99);
        signature = pairOf(187, 88);
        signatureIsValid = crypt.egCheckSignature(message, signature, module, g, y);
        assertFalse(signatureIsValid);
    }

    private static Pair<BigInteger> pairOf(int first, int second) {
        return new Pair<>(valueOf(first), valueOf(second));
    }
}
