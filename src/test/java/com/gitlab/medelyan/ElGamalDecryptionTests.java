package com.gitlab.medelyan;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class ElGamalDecryptionTests extends TestBase {

    @Test
    public void test_allAreValid_fromLecture() {
        BigInteger module = BigInteger.valueOf(54709);
        BigInteger g = BigInteger.valueOf(23113);
        BigInteger y = BigInteger.valueOf(25058);
        BigInteger a = BigInteger.valueOf(926);
        BigInteger b = BigInteger.valueOf(16005);

        BigInteger gammaBase = crypt.egDecrypt(a, b, module, g, y);
        int[] gamma = crypt.extractGamma(gammaBase);
        String encrypted = "фсйощгмпа";

        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        String actual = crypt.decrypt(encrypted, gamma);

        String expected = "спектакль";
        assertEquals(expected, actual);
    }
}
