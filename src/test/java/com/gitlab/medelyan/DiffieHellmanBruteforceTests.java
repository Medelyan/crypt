package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.AlphabetIsNotInitializedException;
import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import com.gitlab.medelyan.exceptions.UnsuccessfulBruteforceException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class DiffieHellmanBruteforceTests extends TestBase {

    @Test
    public void test_encrypted_isNull() throws UnsuccessfulBruteforceException {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        String encrypted = null;

        crypt.dhBruteforce(encrypted, anyValidBase(), anyValidModule(), anyValidPower(), anyValidPower());
    }

    @Test
    public void test_module_isLessThan_2() throws UnsuccessfulBruteforceException {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger module = BigInteger.ONE;

        crypt.dhBruteforce(anyValidText(), anyValidBase(), module, anyValidPower(), anyValidPower());
    }

    @Test
    public void test_base_isLessThan_1() throws UnsuccessfulBruteforceException {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.NOT_SUPPORTED);
        BigInteger base = BigInteger.ZERO;

        crypt.dhBruteforce(anyValidText(), base, anyValidModule(), anyValidPower(), anyValidPower());
    }

    @Test
    public void test_x_isLessThan_1() throws UnsuccessfulBruteforceException {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger x = BigInteger.ZERO;

        crypt.dhBruteforce(anyValidText(), anyValidBase(), anyValidModule(), x, anyValidPower());
    }

    @Test
    public void test_y_isLessThan_1() throws UnsuccessfulBruteforceException {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger y = BigInteger.ZERO;

        crypt.dhBruteforce(anyValidText(), anyValidBase(), anyValidModule(), anyValidPower(), y);
    }

    @Test
    public void test_encrypted_isEmpty() throws UnsuccessfulBruteforceException {
        String encrypted = "";

        String actual = crypt.dhBruteforce(encrypted, anyValidBase(), anyValidModule(), anyValidPower(), anyValidPower());

        String expected = "";
        assertEquals(expected, actual);
    }

    @Test
    public void test_allValid_butUnsolvable() throws UnsuccessfulBruteforceException {
        expected.expect(UnsuccessfulBruteforceException.class);
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        String encrypted = "абабаб";

        crypt.dhBruteforce(encrypted, anyValidBase(), anyValidModule(), anyValidPower(), anyValidPower());
    }

    @Test
    public void test_allValid_solvable_alphabetIsNotInitialized() throws UnsuccessfulBruteforceException {
        expected.expect(AlphabetIsNotInitializedException.class);

        BigInteger base = new BigInteger("10109");
        BigInteger module = new BigInteger("41903");
        BigInteger x = new BigInteger("38421");
        BigInteger y = new BigInteger("14092");
        String encrypted = "всупнхчхуауспз";

        crypt.dhBruteforce(encrypted, base, module, x, y);
    }

    @Test
    public void test_allValid_taskFromLecture() throws UnsuccessfulBruteforceException {
        BigInteger base = new BigInteger("10109");
        BigInteger module = new BigInteger("41903");
        BigInteger x = new BigInteger("38421");
        BigInteger y = new BigInteger("14092");

        String encrypted = "всупнхчхуауспз";
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        String actual = crypt.dhBruteforce(encrypted, base, module, x, y);

        String expected = "биоинформатика";
        assertEquals(expected, actual);
    }
}
