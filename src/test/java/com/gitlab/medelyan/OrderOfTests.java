package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import com.gitlab.medelyan.exceptions.GroupElementIsTooBigException;
import com.gitlab.medelyan.exceptions.InfiniteOrderException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class OrderOfTests extends TestBase {

    @Test
    public void test_groupElementIsLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger groupElement = BigInteger.ONE;

        crypt.orderOf(groupElement, anyValidModule());
    }

    @Test
    public void test_moduleIsLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger module = BigInteger.ONE;

        crypt.orderOf(anyValid(), module);
    }

    @Test
    public void test_groupElementIsNotLessThanModule() {
        expected.expect(GroupElementIsTooBigException.class);
        BigInteger groupElement = new BigInteger("11");
        BigInteger module = new BigInteger("10");

        crypt.orderOf(groupElement, module);
    }

    @Test
    public void test_allValid_orderIsInfinite() {
        expected.expect(InfiniteOrderException.class);
        BigInteger groupElement = new BigInteger("2");
        BigInteger module = new BigInteger("4");

        crypt.orderOf(groupElement, module);
    }

    @Test
    public void test_groupElemet_valid_module_valid() {
        BigInteger groupElement = new BigInteger("3");
        BigInteger module = new BigInteger("7");

        BigInteger actual = crypt.orderOf(groupElement, module);

        BigInteger expected = new BigInteger("6");
        assertEquals(expected, actual);
    }

    @Test
    public void test_groupElement_isPrimitive_module_isValid() {
        BigInteger groupElement = new BigInteger("6");
        BigInteger module = new BigInteger("41");

        BigInteger actual = crypt.orderOf(groupElement, module);

        BigInteger expected = new BigInteger("40");
        assertEquals(expected, actual);
    }
}
