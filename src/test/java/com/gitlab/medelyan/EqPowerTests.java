package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import com.gitlab.medelyan.exceptions.EquationHasNoSolutionsException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class EqPowerTests extends TestBase {

    @Test
    public void test_base_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger base = BigInteger.ONE;

        crypt.eqPower(anyValidPower(), base, anyValidModule());
    }

    @Test
    public void test_module_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger module = BigInteger.ONE;

        crypt.eqPower(anyValidPower(), anyValidBase(), module);
    }

    @Test
    public void test_allValid_unsolvable() {
        expected.expect(EquationHasNoSolutionsException.class);
        BigInteger x = new BigInteger("15");
        BigInteger base = new BigInteger("6");
        BigInteger module = new BigInteger("13");

        crypt.eqPower(x, base, module);
    }

    @Test
    public void test_7_equalTo_6_toPowerOfAlpha_mod_13() {
        BigInteger x = new BigInteger("7");
        BigInteger base = new BigInteger("6");
        BigInteger module = new BigInteger("13");

        BigInteger actual = crypt.eqPower(x, base, module);

        BigInteger expected = new BigInteger("7");
        assertEquals(expected, actual);
    }
}
