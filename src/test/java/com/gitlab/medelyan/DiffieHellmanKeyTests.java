package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class DiffieHellmanKeyTests extends TestBase {

    @Test
    public void test_base_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger base = BigInteger.ONE;

        crypt.dhKey(base, anyValidPower(), anyValidPower(), anyValidModule());
    }

    @Test
    public void test_alpha_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger alpha = BigInteger.ZERO;

        crypt.dhKey(anyValidBase(), alpha, anyValidPower(), anyValidModule());
    }

    @Test
    public void test_beta_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger beta = BigInteger.ZERO;

        crypt.dhKey(anyValidBase(), anyValidPower(), beta, anyValidModule());
    }

    @Test
    public void test_module_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger module = BigInteger.ONE;

        crypt.dhKey(anyValidBase(), anyValidPower(), anyValidPower(), module);
    }

    @Test
    public void test_allAreValid() {
        BigInteger base = new BigInteger("10109");
        BigInteger alpha = new BigInteger("21769");
        BigInteger beta = new BigInteger("17809");
        BigInteger module = new BigInteger("41903");

        BigInteger actual = crypt.dhKey(base, alpha, beta, module);

        BigInteger expected = new BigInteger("19570");
        assertEquals(expected, actual);
    }
}
