package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.AlphabetIsNotInitializedException;
import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import com.gitlab.medelyan.exceptions.UnknownCharException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GammaDecryptionTests extends TestBase {

    @Test
    public void test_text_isNull() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        crypt.decrypt(null, anyValidGamma());
    }

    @Test
    public void test_gamma_isNull() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        crypt.decrypt(anyValidText(), null);
    }

    @Test
    public void test_text_isEmpty() {
        String actual = crypt.decrypt("", anyValidGamma());
        assertEquals("", actual);
    }

    @Test
    public void test_gamma_isEmpty() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.EMPTY_GAMMA);
        int[] gamma = {};

        crypt.decrypt(anyValidText(), gamma);
    }

    @Test
    public void test_text_isValid_alphabet_isNotInitialized() {
        expected.expect(AlphabetIsNotInitializedException.class);

        crypt.decrypt(anyValidText(), anyValidGamma());
    }

    @Test
    public void test_text_containsSymbolsNotFromAlphabet() {
        expected.expect(UnknownCharException.class);
        String alphabet = "#?@";
        crypt.setAlphabet(alphabet);

        crypt.decrypt(anyValidText(), anyValidGamma());
    }

    @Test
    public void test_text_isValid_Caesar() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        int[] singleGamma = crypt.gamma(1);
        String text = "абвг";

        String actual = crypt.encrypt(text, singleGamma);

        String expected = "бвгд";
        assertEquals(expected, actual);
    }

    @Test
    public void test_text_isValid_gamma_isValid() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        String encrypted = "сымцйоямжйр";
        int[] gamma = crypt.gamma(30, 14, 4, 20, 4);

        String actual = crypt.decrypt(encrypted, gamma);

        String expected = "университет";
        assertEquals(expected, actual);
    }

    @Test
    public void test_text_isValid_gamma_containsNumbersLargerThanAlphabetLength() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        int[] singleGamma = crypt.gamma(1 + Alphabets.RUSSIAN_LOWERCASE.length() * 10);
        String encrypted = "бвгд";

        String actual = crypt.decrypt(encrypted, singleGamma);

        String expected = "абвг";
        assertEquals(expected, actual);
    }
}
