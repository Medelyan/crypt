package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SetAlphabetTests extends TestBase {

    @Test
    public void test_alphabet_isNull() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        String alphabet = null;

        crypt.setAlphabet(alphabet);
    }

    @Test
    public void test_alphabet_isEmpty() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.EMPTY_ALPHABET);
        String alphabet = "";

        crypt.setAlphabet(alphabet);
    }

    @Test
    public void test_alphabet_containsRepeatingSymbols() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.REPEATING_SYMBOLS);

        String alphabet = "dd";
        crypt.setAlphabet(alphabet);
    }

    @Test
    public void test_alphabet_isValid() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        String actual = crypt.getAlphabet();

        assertEquals(Alphabets.RUSSIAN_LOWERCASE, actual);
    }

}
