package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class GammaShortcutTests extends TestBase {

    @Test
    public void test_absentParams() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.EMPTY_GAMMA);
        int[] gamma = crypt.gamma();


        int expected = 0;
        assertEquals(expected, gamma.length);
    }

    @Test
    @SuppressWarnings("NullArgumentToVariableArgMethod")
    public void test_nullParam() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        int[] offsets = null;

        crypt.gamma(offsets);
    }

    @Test
    public void test_negativeValues() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.NEGATIVE_OFFSET);
        int offset = -11;

        crypt.gamma(offset);
    }

    @Test
    public void test_validParams() {
        int[] actual = crypt.gamma(1, 2, 3);

        int[] expecteds = {1, 2, 3};
        assertArrayEquals(expecteds, actual);
    }
}
