package com.gitlab.medelyan;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class ShnorrSignatureTests extends TestBase {

    @Test
    public void test_checkSignature_fromLecture() {
        BigInteger p = BigInteger.valueOf(33107);
        BigInteger q = BigInteger.valueOf(16553);
        BigInteger g = BigInteger.valueOf(2902);
        BigInteger y = BigInteger.valueOf(9107);
        BigInteger r = BigInteger.valueOf(32607);
        StringBuilder numbersOfRightSign = new StringBuilder();

        BigInteger e = BigInteger.valueOf(15776);
        BigInteger s = BigInteger.valueOf(9856);
        if (crypt.schnorrCheckSignature(p, q, g, y, r, e, s)) {
            numbersOfRightSign.append("1");
        }

        e = BigInteger.valueOf(490);
        s = BigInteger.valueOf(8108);
        if (crypt.schnorrCheckSignature(p, q, g, y, r, e, s)) {
            numbersOfRightSign.append("2");
        }

        e = BigInteger.valueOf(9987);
        s = BigInteger.valueOf(7309);
        if (crypt.schnorrCheckSignature(p, q, g, y, r, e, s)) {
            numbersOfRightSign.append("3");
        }

        e = BigInteger.valueOf(155);
        s = BigInteger.valueOf(1267);
        if (crypt.schnorrCheckSignature(p, q, g, y, r, e, s)) {
            numbersOfRightSign.append("4");
        }

        String expected = "124";
        assertEquals(expected, numbersOfRightSign.toString());
    }

    @Test
    public void test_schnorrSign_fromLecture() {
        BigInteger q = BigInteger.valueOf(16553);
        BigInteger k = BigInteger.valueOf(14004);
        BigInteger a = BigInteger.valueOf(15643);

        BigInteger randomSeed = BigInteger.valueOf(15776);
        BigInteger actual = crypt.schnorrSign(q, a, k, randomSeed);
        BigInteger expected = BigInteger.valueOf(9856);
        assertEquals(expected, actual);

        randomSeed = BigInteger.valueOf(490);
        actual = crypt.schnorrSign(q, a, k, randomSeed);
        expected = BigInteger.valueOf(8108);
        assertEquals(expected, actual);

        randomSeed = BigInteger.valueOf(155);
        actual = crypt.schnorrSign(q, a, k, randomSeed);
        expected = BigInteger.valueOf(1267);
        assertEquals(expected, actual);
    }
}
