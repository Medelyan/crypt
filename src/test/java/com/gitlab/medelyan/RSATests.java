package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.UnsuccessfulCommonModuleAttackException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.*;

public class RSATests extends TestBase {

    @Test
    public void test_rsaDecrypt() {
        crypt.setAlphabet(Alphabets.RUSSIAN_UPPERCASE);

        BigInteger r = BigInteger.valueOf(71361259);
        BigInteger b = BigInteger.valueOf(74671);
        BigInteger encryptedGamma = BigInteger.valueOf(3942877);
        String message = "ДМТЭЕСХ";

        String expected = "БЕЛФАСТ";
        int[] gamma = crypt.extractGamma(crypt.rsaDecrypt(encryptedGamma, r, b));
        String actual = crypt.decrypt(message, gamma);
        assertEquals(expected, actual);
    }

    @Test
    public void test_rsaEncrypt() {
        crypt.setAlphabet(Alphabets.RUSSIAN_UPPERCASE);

        BigInteger r = BigInteger.valueOf(71361259);
        BigInteger b = BigInteger.valueOf(74671);
        BigInteger message = BigInteger.valueOf(37795031);

        BigInteger actual = crypt.rsaEncrypt(message, r, b);

        BigInteger expected = BigInteger.valueOf(3942877);
        assertEquals(expected, actual);
    }

    @Test
    public void test_rsaCommonModuleAttack() throws UnsuccessfulCommonModuleAttackException {
        BigInteger commonModule = BigInteger.valueOf(399799);
        BigInteger a = BigInteger.valueOf(4397);
        BigInteger b = BigInteger.valueOf(7517);
        BigInteger m1 = BigInteger.valueOf(268100);
        BigInteger m2 = BigInteger.valueOf(263851);

        BigInteger expected = BigInteger.valueOf(237135);
        BigInteger actual = crypt.rsaCommonModuleAttack(commonModule, a, b, m1, m2);
        assertEquals(expected, actual);
    }
}
