package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.AIsNotPrimeToModuleException;
import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class EqSolveTests extends TestBase {

    @Test
    public void test_a_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.NOT_SUPPORTED);
        BigInteger a = BigInteger.ZERO;
        BigInteger b = anyValid();
        BigInteger module = anyValidModule();

        crypt.eqSolve(a, b, module);
    }

    @Test
    public void test_a_isLessThan_1_eulerOfModule() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.NOT_SUPPORTED);
        BigInteger a = BigInteger.ZERO;
        BigInteger b = anyValid();
        BigInteger module = anyValidModule();
        BigInteger eulerOfModule = anyValid();

        crypt.eqSolve(a, b, module, eulerOfModule);
    }

    @Test
    public void test_b_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.NOT_SUPPORTED);
        BigInteger a = anyValid();
        BigInteger b = BigInteger.ZERO;
        BigInteger module = anyValidModule();

        crypt.eqSolve(a, b, module);
    }

    @Test
    public void test_b_isLessThan_1_eulerOfModule() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.NOT_SUPPORTED);
        BigInteger a = anyValid();
        BigInteger b = BigInteger.ZERO;
        BigInteger module = anyValidModule();
        BigInteger eulerOfModule = anyValid();

        crypt.eqSolve(a, b, module, eulerOfModule);
    }

    @Test
    public void test_module_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger a = anyValid();
        BigInteger b = anyValid();
        BigInteger module = BigInteger.ONE;

        crypt.eqSolve(a, b, module);
    }

    @Test
    public void test_module_isLessThan_2_eulerOfModule() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger a = anyValid();
        BigInteger b = anyValid();
        BigInteger module = BigInteger.ONE;
        BigInteger eulerOfModule = anyValid();

        crypt.eqSolve(a, b, module, eulerOfModule);
    }

    @Test
    public void test_eulerOfModule_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger a = anyValid();
        BigInteger b = anyValid();
        BigInteger module = anyValidModule();
        BigInteger eulerOfModule = BigInteger.ZERO;

        crypt.eqSolve(a, b, module, eulerOfModule);
    }

    @Test
    public void test_allValid_a_isNotPrimeTo_module() {
        expected.expect(AIsNotPrimeToModuleException.class);
        BigInteger a = new BigInteger("25");
        BigInteger b = anyValid();
        BigInteger module = BigInteger.TEN;

        crypt.eqSolve(a, b, module);
    }

    @Test
    public void test_allValid_a_isNotPrimeTo_module_eulerOfModule() {
        expected.expect(AIsNotPrimeToModuleException.class);
        BigInteger a = new BigInteger("25");
        BigInteger b = anyValid();
        BigInteger module = BigInteger.TEN;
        BigInteger eulerOfModule = crypt.euler(module);

        crypt.eqSolve(a, b, module, eulerOfModule);
    }

    @Test
    public void test_a_is_25_b_is_6_module_is_14() {
        BigInteger a = new BigInteger("25");
        BigInteger b = new BigInteger("6");
        BigInteger module = new BigInteger("14");

        BigInteger actual = crypt.eqSolve(a, b, module);

        BigInteger expected = new BigInteger("12");
        assertEquals(expected, actual);
    }

    @Test
    public void test_a_is_25_b_is_6_module_is_14_eulerOfModule() {
        BigInteger a = new BigInteger("25");
        BigInteger b = new BigInteger("6");
        BigInteger module = new BigInteger("14");
        BigInteger eulerOfModule = crypt.euler(module);

        BigInteger actual = crypt.eqSolve(a, b, module, eulerOfModule);

        BigInteger expected = new BigInteger("12");
        assertEquals(expected, actual);
    }

    @Test
    public void test_taskFromLecture() {
        BigInteger module = BigInteger.valueOf(62_923_926_601L);
        BigInteger b = BigInteger.valueOf(678_433L);
        BigInteger m3 = BigInteger.valueOf(34_979_060_413L);
        BigInteger eulerOfModule = module.subtract(BigInteger.ONE);

        BigInteger beta = crypt.eqSolve(b, BigInteger.ONE, eulerOfModule);
        BigInteger m = m3.modPow(beta, module);

        String actual = decrypt(m.toString());

        String expected = "океан";
        assertEquals(expected, actual);
    }

    private static String decrypt(String m) {
        if (m.length() % 2 != 0) {
            return "ERROR: length is not even";
        }

        StringBuilder result = new StringBuilder();
        String current = m;
        while (current.length() > 0) {
            String value = current.substring(0, 2);
            if (current.length() > 2) {
                current = current.substring(2);
            } else {
                current = "";
            }
            int index = Integer.parseInt(value) - 10;
            result.append(Alphabets.RUSSIAN_LOWERCASE.charAt(index));
        }
        return result.toString();
    }
}
