package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class EulerTests extends TestBase {

    @Test
    public void test_value_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger value = BigInteger.ZERO;

        crypt.euler(value);
    }

    @Test
    public void test_value_is_1() {
        BigInteger value = BigInteger.ONE;

        BigInteger actual = crypt.euler(value);

        BigInteger expected = BigInteger.ONE;
        assertEquals(expected, actual);
    }

    @Test
    public void test_value_is_69() {
        BigInteger value = new BigInteger("69");

        BigInteger actual = crypt.euler(value);

        BigInteger expected = new BigInteger("44");
        assertEquals(expected, actual);
    }
}
