package com.gitlab.medelyan;

import org.junit.Test;

import java.math.BigInteger;

import static java.math.BigInteger.valueOf;
import static org.junit.Assert.assertEquals;

public class ElGamalSignTests extends TestBase {
    @Test
    public void test_allValid_fromLecture() {
        BigInteger module = valueOf(23);
        BigInteger g = valueOf(5);
        BigInteger xSeed = BigInteger.valueOf(7);
        BigInteger kSeed = BigInteger.valueOf(5);
        BigInteger message = BigInteger.valueOf(3);

        Pair<BigInteger> actual = crypt.egSign(message, xSeed, kSeed, module, g);

        BigInteger expectedR = BigInteger.valueOf(20);
        BigInteger expectedS = BigInteger.valueOf(21);
        assertEquals(expectedR, actual.getFirst());
        assertEquals(expectedS, actual.getSecond());
    }
}
