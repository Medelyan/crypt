package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import java.math.BigInteger;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;

public class PrimitiveElementsSearchTests extends TestBase {

    @Test
    public void test_module_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger module = BigInteger.ONE;

        crypt.primitivesBy(module);
    }

    @Test
    public void test_module_isEqualTo_2() {
        BigInteger module = new BigInteger("2");

        BigInteger[] actual = crypt.primitivesBy(module);

        BigInteger[] expected = {};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void test_module_isValid() {
        BigInteger module = new BigInteger("41");

        BigInteger[] actual = crypt.primitivesBy(module);

        BigInteger[] expected = new BigInteger[]{
                new BigInteger("6"),
                new BigInteger("7"),
                new BigInteger("11"),
                new BigInteger("12"),
                new BigInteger("13"),
                new BigInteger("15"),
                new BigInteger("17"),
                new BigInteger("19"),
                new BigInteger("22"),
                new BigInteger("24"),
                new BigInteger("26"),
                new BigInteger("28"),
                new BigInteger("29"),
                new BigInteger("30"),
                new BigInteger("34"),
                new BigInteger("35")};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void test() {
        BigInteger module = new BigInteger("4");

        BigInteger[] actual = crypt.primitivesBy(module);

        BigInteger[] expected = new BigInteger[]{};
        System.out.println(Arrays.toString(actual));
        assertArrayEquals(expected, actual);
    }
}
