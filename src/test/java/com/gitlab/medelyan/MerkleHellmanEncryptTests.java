package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.AlphabetIsNotInitializedException;
import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import com.gitlab.medelyan.exceptions.EncoderIsNotInitializedException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class MerkleHellmanEncryptTests extends TestBase {

    @Test
    public void test_allValid() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
        String text = "август";

        BigInteger[] publicKey = crypt.mhPublicKey(validMHPrivateKey());

        BigInteger[] expected = new BigInteger[] {
                BigInteger.valueOf(55),
                BigInteger.valueOf(97),
                BigInteger.valueOf(21),
                BigInteger.valueOf(79),
                BigInteger.valueOf(100),
                BigInteger.valueOf(155),
        };

        BigInteger[] actual = crypt.mhEncrypt(text, publicKey);

        assertArrayEquals(expected, actual);
    }

    @Test
    public void test_TextIsNull() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        String text = null;

        BigInteger[] publicKey = crypt.mhPublicKey(validMHPrivateKey());

        crypt.mhEncrypt(text, publicKey);
    }

    @Test
    public void test_AlphabetIsNotSet() {
        expected.expect(AlphabetIsNotInitializedException.class);

        BigInteger[] publicKey = crypt.mhPublicKey(validMHPrivateKey());
        crypt.mhEncrypt(anyValidText(), publicKey);
    }

    @Test
    public void test_TextIsEmpty() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        BigInteger[] expected = new BigInteger[]{};
        BigInteger[] actual = crypt.mhEncrypt("", validMHPublicKey());
        assertArrayEquals(expected, actual);
    }
}
