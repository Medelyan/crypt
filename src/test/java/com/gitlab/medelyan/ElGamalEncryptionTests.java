package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.*;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;


public class ElGamalEncryptionTests extends TestBase {

    @Test
    public void test_message_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger message = BigInteger.ZERO;

        crypt.egEncrypt(message, anyValidModule(), anyValid(), anyValid(), anyValid());
    }

    @Test
    public void test_message_isGreaterOrEqualsToModule() {
        expected.expect(MessageIsGreaterThanModuleException.class);
        BigInteger message = BigInteger.valueOf(11);
        BigInteger module = BigInteger.valueOf(11);

        crypt.egEncrypt(message, module, anyValid(), anyValid(), anyValid());
    }

    @Test
    public void test_module_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger module = BigInteger.ONE;

        crypt.egEncrypt(anyValid(), module, anyValid(), anyValid(), anyValid());
    }

    @Test
    public void test_g_isLessThan_2() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger g = BigInteger.ONE;

        crypt.egEncrypt(anyValid(), anyValidModule(), g, anyValid(), anyValid());
    }

    @Test
    public void test_g_isGreaterOrEqualsToModule() {
        expected.expect(GIsGreaterOrEqualsToModuleException.class);
        BigInteger module = BigInteger.valueOf(11);
        BigInteger g = BigInteger.valueOf(11);

        crypt.egEncrypt(anyValid(), module, g, anyValid(), anyValid());
    }

    @Test
    public void test_g_isNotPrimitive() {
        expected.expect(GIsNotPrimitiveException.class);
        BigInteger module = BigInteger.valueOf(11);
        BigInteger g = BigInteger.valueOf(10);

        crypt.egEncrypt(anyValid(), module, g, anyValid(), anyValid());
    }

    @Test
    public void test_y_isLessThan_0() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger y = BigInteger.valueOf(-1);

        crypt.egEncrypt(anyValid(), anyValidModule(), anyValid(), y, anyValid());
    }

    @Test
    public void test_randomSeed_isLessThan_1() {
        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_VALUE);
        BigInteger randomSeed = BigInteger.ZERO;

        crypt.egEncrypt(anyValid(), anyValidModule(), anyValid(), anyValid(), randomSeed);
    }

    @Test
    public void test_randomSeed_isGreaterOrEqualsToModule() {
        expected.expect(RandomSeedIsGreaterOrEqualsToModuleException.class);
        BigInteger module = BigInteger.valueOf(11);
        BigInteger primitiveG = BigInteger.valueOf(2);
        BigInteger randomSeed = BigInteger.valueOf(11);

        crypt.egEncrypt(anyValid(), module, primitiveG, anyValid(), randomSeed);
    }

    @Test
    public void test_allAreValid_canDecryptWhatsBeenEncrypted() {
        BigInteger module = BigInteger.valueOf(54709);
        BigInteger g = BigInteger.valueOf(23113);
        BigInteger y = BigInteger.valueOf(25058);
        BigInteger message = BigInteger.valueOf(32447);
        BigInteger randomSeed = BigInteger.valueOf(17896);

        Pair<BigInteger> encrypted = crypt.egEncrypt(message, module, g, y, randomSeed);

        BigInteger expectedA = BigInteger.valueOf(926);
        BigInteger expectedB = BigInteger.valueOf(16005);
        assertEquals(expectedA, encrypted.getFirst());
        assertEquals(expectedB, encrypted.getSecond());

        BigInteger decrypted = crypt.egDecrypt(encrypted.getFirst(), encrypted.getSecond(), module, g, y);
        assertEquals(message, decrypted);
    }
}
