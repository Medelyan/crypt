package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.AlphabetIsNotInitializedException;
import com.gitlab.medelyan.exceptions.ConstraintViolationException;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class MerkleHellmanDecryptTests extends TestBase {

    @Test
    public void test_allValid() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        BigInteger[] encrypted = getValidEncrypted();
        BigInteger[] seq = validMHSequence();
        BigInteger module = validMHModule();
        BigInteger coef = validMHCoefficient();
        MHPrivateKey key = new MHPrivateKey(seq, module, coef);

        String actual = crypt.mhDecrypt(encrypted, key);

        String expected = "август";
        assertEquals(expected, actual);
    }

    @Test
    public void test_SequenceNotValid() {
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_MH_PRIVATE_KEY);

        BigInteger[] seq = new BigInteger[] {
                BigInteger.valueOf(1),
                BigInteger.valueOf(2),
                BigInteger.valueOf(1),
                BigInteger.valueOf(9),
                BigInteger.valueOf(8),
                BigInteger.valueOf(35),
        };
        BigInteger module = validMHModule();
        BigInteger coef = validMHCoefficient();
        MHPrivateKey privateKey = new MHPrivateKey(seq, module, coef);

        BigInteger[] encrypted = getValidEncrypted();
        crypt.mhDecrypt(encrypted, privateKey);
    }

    @Test
    public void test_ModuleLessThanCoefficient(){
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_MH_PRIVATE_KEY);

        BigInteger[] seq = validMHSequence();
        BigInteger module = BigInteger.valueOf(23);
        BigInteger coef = BigInteger.valueOf(25);

        BigInteger[] encrypted = getValidEncrypted();
        crypt.mhDecrypt(encrypted, new MHPrivateKey(seq, module, coef));
    }

    @Test
    public void test_ModuleAndCoefficientAreNotMutuallyPrime(){
        crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

        expected.expect(ConstraintViolationException.class);
        expected.expectMessage(Crypt.BAD_MH_PRIVATE_KEY);

        BigInteger[] seq = validMHSequence();
        BigInteger module = BigInteger.valueOf(25);
        BigInteger coef = BigInteger.valueOf(5);

        crypt.mhDecrypt(seq, new MHPrivateKey(seq, module, coef));
    }

    @Test
    public void test_AlphabetIsNotSet() {
        expected.expect(AlphabetIsNotInitializedException.class);

        BigInteger[] encrypted = getValidEncrypted();
        BigInteger[] seq = validMHSequence();
        BigInteger module = validMHModule();
        BigInteger coef = validMHCoefficient();
        MHPrivateKey key = new MHPrivateKey(seq, module, coef);

        crypt.mhDecrypt(encrypted, key);
    }

    private BigInteger[] getValidEncrypted() {
        return new BigInteger[] {
                BigInteger.valueOf(55),
                BigInteger.valueOf(97),
                BigInteger.valueOf(21),
                BigInteger.valueOf(79),
                BigInteger.valueOf(100),
                BigInteger.valueOf(155),
        };
    }
}
