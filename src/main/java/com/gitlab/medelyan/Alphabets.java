package com.gitlab.medelyan;

@SuppressWarnings("WeakerAccess")
public final class Alphabets {
    public static final String RUSSIAN_LOWERCASE = "абвгдежзийклмнопрстуфхцчшщъыьэюя";
    public static final String RUSSIAN_UPPERCASE = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ";


    public static final String ENGLISH_LOWERCASE = "abcdefghijklmnopqrstuvwxyz";
    public static final String ENGLISH_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private Alphabets() {

    }
}
