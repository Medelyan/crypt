package com.gitlab.medelyan.exceptions;

public class GroupElementIsTooBigException extends RuntimeException {
    public GroupElementIsTooBigException() {
    }

    public GroupElementIsTooBigException(String message) {
        super(message);
    }

    public GroupElementIsTooBigException(String message, Throwable cause) {
        super(message, cause);
    }

    public GroupElementIsTooBigException(Throwable cause) {
        super(cause);
    }

    public GroupElementIsTooBigException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
