package com.gitlab.medelyan.exceptions;

public class UnsuccessfulBruteforceException extends Exception {
    public UnsuccessfulBruteforceException() {
    }

    public UnsuccessfulBruteforceException(String message) {
        super(message);
    }

    public UnsuccessfulBruteforceException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsuccessfulBruteforceException(Throwable cause) {
        super(cause);
    }

    public UnsuccessfulBruteforceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
