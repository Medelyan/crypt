package com.gitlab.medelyan.exceptions;

public class MessageIsGreaterThanModuleException extends RuntimeException {
    public MessageIsGreaterThanModuleException() {
    }

    public MessageIsGreaterThanModuleException(String message) {
        super(message);
    }

    public MessageIsGreaterThanModuleException(String message, Throwable cause) {
        super(message, cause);
    }

    public MessageIsGreaterThanModuleException(Throwable cause) {
        super(cause);
    }

    public MessageIsGreaterThanModuleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
