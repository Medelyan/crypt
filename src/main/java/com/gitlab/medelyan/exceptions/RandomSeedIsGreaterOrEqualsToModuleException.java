package com.gitlab.medelyan.exceptions;

public class RandomSeedIsGreaterOrEqualsToModuleException extends RuntimeException {
    public RandomSeedIsGreaterOrEqualsToModuleException() {
    }

    public RandomSeedIsGreaterOrEqualsToModuleException(String message) {
        super(message);
    }

    public RandomSeedIsGreaterOrEqualsToModuleException(String message, Throwable cause) {
        super(message, cause);
    }

    public RandomSeedIsGreaterOrEqualsToModuleException(Throwable cause) {
        super(cause);
    }

    public RandomSeedIsGreaterOrEqualsToModuleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
