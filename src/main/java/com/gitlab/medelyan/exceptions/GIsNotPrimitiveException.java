package com.gitlab.medelyan.exceptions;

public class GIsNotPrimitiveException extends RuntimeException {
    public GIsNotPrimitiveException() {
    }

    public GIsNotPrimitiveException(String message) {
        super(message);
    }

    public GIsNotPrimitiveException(String message, Throwable cause) {
        super(message, cause);
    }

    public GIsNotPrimitiveException(Throwable cause) {
        super(cause);
    }

    public GIsNotPrimitiveException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
