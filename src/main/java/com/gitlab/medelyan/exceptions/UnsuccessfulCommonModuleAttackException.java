package com.gitlab.medelyan.exceptions;

public class UnsuccessfulCommonModuleAttackException extends Exception {
    public UnsuccessfulCommonModuleAttackException() {
    }

    public UnsuccessfulCommonModuleAttackException(String message) {
        super(message);
    }

    public UnsuccessfulCommonModuleAttackException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsuccessfulCommonModuleAttackException(Throwable cause) {
        super(cause);
    }

    public UnsuccessfulCommonModuleAttackException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
