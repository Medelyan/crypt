package com.gitlab.medelyan.exceptions;

public class InfiniteOrderException extends RuntimeException {
    public InfiniteOrderException() {
    }

    public InfiniteOrderException(String message) {
        super(message);
    }

    public InfiniteOrderException(String message, Throwable cause) {
        super(message, cause);
    }

    public InfiniteOrderException(Throwable cause) {
        super(cause);
    }

    public InfiniteOrderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
