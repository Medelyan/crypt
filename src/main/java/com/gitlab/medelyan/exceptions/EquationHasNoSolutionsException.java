package com.gitlab.medelyan.exceptions;

public class EquationHasNoSolutionsException extends RuntimeException {
    public EquationHasNoSolutionsException() {
    }

    public EquationHasNoSolutionsException(String message) {
        super(message);
    }

    public EquationHasNoSolutionsException(String message, Throwable cause) {
        super(message, cause);
    }

    public EquationHasNoSolutionsException(Throwable cause) {
        super(cause);
    }

    public EquationHasNoSolutionsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
