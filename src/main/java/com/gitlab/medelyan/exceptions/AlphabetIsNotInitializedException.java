package com.gitlab.medelyan.exceptions;

public class AlphabetIsNotInitializedException extends RuntimeException {
    public AlphabetIsNotInitializedException() {
    }

    public AlphabetIsNotInitializedException(String message) {
        super(message);
    }

    public AlphabetIsNotInitializedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AlphabetIsNotInitializedException(Throwable cause) {
        super(cause);
    }

    public AlphabetIsNotInitializedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
