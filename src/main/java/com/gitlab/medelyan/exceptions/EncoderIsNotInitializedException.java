package com.gitlab.medelyan.exceptions;

public class EncoderIsNotInitializedException extends RuntimeException {
    public EncoderIsNotInitializedException() {
    }

    public EncoderIsNotInitializedException(String message) {
        super(message);
    }

    public EncoderIsNotInitializedException(String message, Throwable cause) {
        super(message, cause);
    }

    public EncoderIsNotInitializedException(Throwable cause) {
        super(cause);
    }

    public EncoderIsNotInitializedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
