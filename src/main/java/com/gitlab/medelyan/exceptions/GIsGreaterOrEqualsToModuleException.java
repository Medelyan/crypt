package com.gitlab.medelyan.exceptions;

public class GIsGreaterOrEqualsToModuleException extends RuntimeException {
    public GIsGreaterOrEqualsToModuleException() {
    }

    public GIsGreaterOrEqualsToModuleException(String message) {
        super(message);
    }

    public GIsGreaterOrEqualsToModuleException(String message, Throwable cause) {
        super(message, cause);
    }

    public GIsGreaterOrEqualsToModuleException(Throwable cause) {
        super(cause);
    }

    public GIsGreaterOrEqualsToModuleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
