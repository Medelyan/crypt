package com.gitlab.medelyan.exceptions;

public class UnknownCharException extends RuntimeException {
    public UnknownCharException() {
    }

    public UnknownCharException(String message) {
        super(message);
    }

    public UnknownCharException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownCharException(Throwable cause) {
        super(cause);
    }

    public UnknownCharException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
