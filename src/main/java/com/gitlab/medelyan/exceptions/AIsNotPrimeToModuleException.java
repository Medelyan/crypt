package com.gitlab.medelyan.exceptions;

public class AIsNotPrimeToModuleException extends RuntimeException {
    public AIsNotPrimeToModuleException() {
    }

    public AIsNotPrimeToModuleException(String message) {
        super(message);
    }

    public AIsNotPrimeToModuleException(String message, Throwable cause) {
        super(message, cause);
    }

    public AIsNotPrimeToModuleException(Throwable cause) {
        super(cause);
    }

    public AIsNotPrimeToModuleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
