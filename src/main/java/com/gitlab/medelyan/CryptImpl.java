package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.*;

import static java.math.BigInteger.*;

import java.math.BigInteger;
import java.util.*;

import static java.util.AbstractMap.SimpleEntry;

class CryptImpl implements Crypt {
    private String alphabet;

    CryptImpl() {

    }

    @Override
    public int[] gamma(int... offsets) {
        return offsets;
    }

    @Override
    public String encrypt(String text, int[] gamma) {
        if (text.isEmpty()) {
            return "";
        }

        requireAlphabet();

        StringBuilder result = new StringBuilder();
        int alphabetLength = alphabet.length();

        for (int i = 0; i < text.length(); i++) {
            char encChar = text.charAt(i);
            checkCharIsKnown(encChar);

            int alphabetPos = alphabet.indexOf(encChar);

            // [SM] We shift position forward by gammaOffset when encrypting
            int gammaOffset = gamma[i % gamma.length];
            alphabetPos = (alphabetPos + gammaOffset) % alphabetLength;

            result.append(alphabet.charAt(alphabetPos));
        }

        return result.toString();
    }

    @Override
    public String decrypt(String encrypted, int[] gamma) {
        if (encrypted.isEmpty()) {
            return "";
        }

        requireAlphabet();

        StringBuilder result = new StringBuilder();
        long alphabetLength = alphabet.length();

        for (int i = 0; i < encrypted.length(); i++) {
            char encChar = encrypted.charAt(i);
            checkCharIsKnown(encChar);

            int alphabetPos = alphabet.indexOf(encChar);

            // [SM] We shift position backwards by gammaOffset when decrypting
            int gammaOffset = gamma[i % gamma.length];
            alphabetPos -= gammaOffset % alphabetLength;

            // [SM] Now alphabetPos is in (-alpabetLength;alphabetLength) open range;
            // adjust negative values
            if (alphabetPos < 0) {
                alphabetPos += alphabetLength;
            }

            result.append(alphabet.charAt(alphabetPos));
        }

        return result.toString();
    }

    @Override
    public BigInteger dhKey(BigInteger base, BigInteger alpha, BigInteger beta, BigInteger module) {
        return base.modPow(alpha.multiply(beta), module);
    }

    @Override
    public String dhEncrypt(String text, BigInteger base, BigInteger module, BigInteger alpha, BigInteger beta) {
        if (text.isEmpty()) {
            return "";
        }

        BigInteger x = base.modPow(alpha, module);
        BigInteger key = x.modPow(beta, module);
        int[] gamma = extractGamma(key);

        return encrypt(text, gamma);
    }

    @Override
    public String dhDecrypt(String encrypted, BigInteger base, BigInteger alpha, BigInteger beta, BigInteger module) {
        if (encrypted.isEmpty()) {
            return "";
        }

        BigInteger key = dhKey(base, alpha, beta, module);
        return dhDecrypt(encrypted, key);
    }

    @Override
    public String dhDecrypt(String encrypted, BigInteger key) {
        if (encrypted.isEmpty()) {
            return "";
        }

        int[] gamma = extractGamma(key);
        return decrypt(encrypted, gamma);
    }

    @Override
    public String dhBruteforce(String encrypted, BigInteger base, BigInteger module, BigInteger x, BigInteger y)
            throws UnsuccessfulBruteforceException {
        if (encrypted.isEmpty()) {
            return "";
        }

        BigInteger alpha;
        try {
            alpha = eqPower(x, base, module);
        } catch (EquationHasNoSolutionsException equationHasNoSolutions) {
            throw new UnsuccessfulBruteforceException(equationHasNoSolutions);
        }

        BigInteger key = y.modPow(alpha, module);
        int[] gamma = extractGamma(key);

        return decrypt(encrypted, gamma);
    }

    @Override
    public BigInteger eqPower(BigInteger x, BigInteger base, BigInteger module) {
        BigInteger power = ZERO;
        BigInteger rem = ONE;

        while (power.compareTo(module) < 0) {
            if (rem.equals(x)) {
                return power;
            }

            power = power.add(ONE);
            rem = rem.multiply(base).mod(module);
        }

        throw new EquationHasNoSolutionsException();
    }

    @Override
    public BigInteger orderOf(BigInteger groupElement, BigInteger module) {
        if (groupElement.compareTo(module) >= 0) {
            throw new GroupElementIsTooBigException();
        }

        BigInteger currentOrder = ZERO;
        BigInteger rem;
        do {
            currentOrder = currentOrder.add(ONE);
            rem = groupElement.modPow(currentOrder, module);
        } while (currentOrder.compareTo(module) < 0 && !rem.equals(ONE));

        if (!rem.equals(ONE)) {
            throw new InfiniteOrderException();
        }

        return currentOrder;
    }

    @Override
    public boolean isPrimitive(BigInteger groupElement, BigInteger module) {
        BigInteger k = module.subtract(ONE);

        try {
            return orderOf(groupElement, module).equals(k)
                    && groupElement.modPow(k, module).equals(ONE);
        } catch (InfiniteOrderException ioe) {
            return false;
        }
    }

    @Override
    public BigInteger[] primitivesBy(BigInteger module) {
        List<BigInteger> primitives = new ArrayList<>();

        // [SM]: we iterate through all meaningful and possible
        // candidates
        for (BigInteger i = BigInteger.valueOf(2);
             i.compareTo(module) < 0;
             i = i.add(ONE)) {
            if (isPrimitive(i, module)) {
                primitives.add(i);
            }
        }

        return primitives.toArray(new BigInteger[]{});
    }

    @Override
    public BigInteger euler(BigInteger value) {

        BigInteger number = ZERO;

        // [SM] optimization goes =)
        if (value.compareTo(BigInteger.valueOf(13)) > 0) {
            if (value.mod(BigInteger.valueOf(2)).equals(ZERO)) {
                return optimizedEulerFor(value, BigInteger.valueOf(2));
            }

            if (value.mod(BigInteger.valueOf(3)).equals(ZERO)) {
                return optimizedEulerFor(value, BigInteger.valueOf(3));
            }

            if (value.mod(BigInteger.valueOf(5)).equals(ZERO)) {
                return optimizedEulerFor(value, BigInteger.valueOf(5));
            }

            if (value.mod(BigInteger.valueOf(7)).equals(ZERO)) {
                return optimizedEulerFor(value, BigInteger.valueOf(7));
            }

            if (value.mod(BigInteger.valueOf(11)).equals(ZERO)) {
                return optimizedEulerFor(value, BigInteger.valueOf(11));
            }

            if (value.mod(BigInteger.valueOf(13)).equals(ZERO)) {
                return optimizedEulerFor(value, BigInteger.valueOf(13));
            }
        }

        for (BigInteger i = ONE; i.compareTo(value) <= 0; i = i.add(ONE)) {
            if (i.gcd(value).equals(ONE)) {
                number = number.add(ONE);
            }
        }

        return number;
    }

    private BigInteger optimizedEulerFor(BigInteger value, BigInteger divisor) {
        BigInteger m = value.divide(divisor);
        @SuppressWarnings("UnnecessaryLocalVariable")
        BigInteger n = divisor;
        BigInteger phiM = euler(m);
        BigInteger phiN = euler(n);
        BigInteger gcd = n.gcd(m);
        BigInteger phiGcd = euler(gcd);

        return phiM.multiply(phiN).multiply(gcd).divide(phiGcd);
    }

    @Override
    public BigInteger eqSolve(BigInteger a, BigInteger b, BigInteger module) {
        return eqSolve(a, b, module, euler(module));
    }

    @Override
    public BigInteger eqSolve(BigInteger a, BigInteger b, BigInteger module, BigInteger eulerOfModule) {
        if (!a.gcd(module).equals(ONE)) {
            throw new AIsNotPrimeToModuleException();
        }

        @SuppressWarnings("UnnecessaryLocalVariable")
        BigInteger x = b.multiply(a.modPow(eulerOfModule.subtract(ONE), module)).mod(module);
        return x;
    }

    @Override
    public BigInteger[] mhEncrypt(String text, BigInteger[] publicKey, EncodingCharStrategy encoder) {
        BigInteger[] encrypted = new BigInteger[text.length()];
        if (text.isEmpty()) {
            return encrypted;
        }

        requireAlphabet();

        char[] textChars = text.toCharArray();

        for (int i = 0; i < textChars.length; i++) {
            encrypted[i] = mhEncryptChar(textChars[i], publicKey, encoder);
        }

        return encrypted;
    }

    @Override
    public BigInteger[] mhEncrypt(String text, BigInteger[] publicKey) {
        EncodingCharStrategy encoder = new CharToBinaryNumberEncoder();
        return mhEncrypt(text, publicKey, encoder);
    }

    private BigInteger mhEncryptChar(char ch, BigInteger[] publicKey, EncodingCharStrategy encoder) {
        BigInteger sum = ZERO;
        // [LT] character is encoded as a set of zeros and ones
        String bin = encoder.encode(ch, alphabet);
        // [LT] public key length which should be equal to bin length according to the algorithm
        int fullBinLength = publicKey.length;
        // [LT] number of missing zeros at the beginning of the "bin" so that its length equals to the length of public key
        int diff = fullBinLength - bin.length();

        for (int j = 0; j < bin.length(); j++) {
            if (bin.charAt(j) == '1') {
                sum = sum.add(publicKey[j + diff]);
            }
        }

        return sum;
    }

    @Override
    public BigInteger[] mhPublicKey(MHPrivateKey privateKey) {
        BigInteger[] sequence = privateKey.getSequence();
        BigInteger module = privateKey.getModule();
        BigInteger coef = privateKey.getCoefficient();

        BigInteger[] publicKey = new BigInteger[sequence.length];
        for (int i = 0; i < sequence.length; i++) {
            publicKey[i] = coef.multiply(sequence[i]).mod(module);
        }
        return publicKey;
    }

    @Override
    public String mhDecrypt(BigInteger[] encrypted, MHPrivateKey key, EncodingCharStrategy encoder) {
        if (encrypted.length == 0) {
            return "";
        }

        requireAlphabet();
        BigInteger coef = key.getCoefficient();
        BigInteger module = key.getModule();
        BigInteger[] superSequence = key.getSequence();

        BigInteger inverseCoef = coef.modInverse(module);
        StringBuilder decrypted = new StringBuilder();
        // FIXME: [SM] Consider factoring this out to a method to avoid deep nesting
        for (BigInteger encChar : encrypted) {
            StringBuilder letterBitMask = new StringBuilder();
            BigInteger s = (inverseCoef.multiply(encChar)).remainder(module);
            for (int i = superSequence.length - 1; i >= 0; i--) {
                int bit;
                if (s.compareTo(superSequence[i]) < 0) {
                    bit = 0;
                } else {
                    bit = 1;
                }
                letterBitMask.append(Integer.toBinaryString(bit));
                if (bit == 1) {
                    s = s.subtract(superSequence[i]);
                }
            }
            letterBitMask.reverse();
            decrypted.append(encoder.decode(letterBitMask.toString(), alphabet));
        }

        return decrypted.toString();
    }

    @Override
    public String mhDecrypt(BigInteger[] encrypted, MHPrivateKey key) {
        return mhDecrypt(encrypted, key, new CharToBinaryNumberEncoder());
    }

    @Override
    public BigInteger rsaDecrypt(BigInteger message, BigInteger r, BigInteger b) {
        BigInteger module = euler(r);
        BigInteger power = euler(module).subtract(BigInteger.ONE);
        BigInteger privateKey = b.modPow(power, module); // eqSolve(b, BigInteger.ONE, module);
        return message.modPow(privateKey, r);
    }

    @Override
    public BigInteger rsaEncrypt(BigInteger message, BigInteger r, BigInteger b) {
        return message.modPow(b, r);
    }

    @Override
    public BigInteger rsaCommonModuleAttack(BigInteger commonModule, BigInteger a, BigInteger b, BigInteger m1, BigInteger m2) throws UnsuccessfulCommonModuleAttackException {
        SimpleEntry<BigInteger, BigInteger> xy;
        try {
            xy = solveDiofantineEq(a, b, BigInteger.ONE);
        } catch (EquationHasNoSolutionsException ex) {
            throw new UnsuccessfulCommonModuleAttackException();
        }
        BigInteger x = xy.getKey();
        BigInteger y = xy.getValue();
        BigInteger p1 = m1.modPow(x, commonModule);
        BigInteger p2 = m2.modPow(y, commonModule);

        return p1.multiply(p2).mod(commonModule);
    }

    /**
     * Finds solution (x, y) of Diofantine equation a*x + b*y = c
     *
     * @param a coefficient of x
     * @param b coefficient of y
     * @param c free coefficient
     * @return pair of x, y
     */
    private SimpleEntry<BigInteger, BigInteger> solveDiofantineEq(BigInteger a, BigInteger b, BigInteger c) throws EquationHasNoSolutionsException {
        if (!a.gcd(b).equals(BigInteger.ONE)) {
            throw new EquationHasNoSolutionsException();
        }
        BigInteger x = eqSolve(a, c, b);
        BigInteger y = (c.subtract(a.multiply(x))).divide(b);
        return new SimpleEntry<>(x, y);
    }

    @Override
    public boolean schnorrCheckSignature(BigInteger p, BigInteger q, BigInteger g, BigInteger y, BigInteger r, BigInteger e, BigInteger s) {
        BigInteger n = r.subtract(g.modPow(s, p).multiply(y.modPow(e, p))).mod(p);
        return n.equals(ZERO);
    }

    @Override
    public BigInteger schnorrSign(BigInteger q, BigInteger a, BigInteger k, BigInteger randomSeed) {
        return k.multiply(randomSeed).add(a).mod(q);
    }

    private void checkCharIsKnown(char c) {
        if (alphabet.indexOf(c) == -1) {
            throw new UnknownCharException("'" + c + "' is not present in the alphabet: '" +
                    alphabet + "'");
        }
    }

    private void requireAlphabet() {
        if (alphabet == null) {
            throw new AlphabetIsNotInitializedException();
        }
    }

    @Override
    public int[] extractGamma(BigInteger key) {
        List<Integer> gamma = new ArrayList<>();

        while (key.compareTo(BigInteger.ZERO) > 0) {
            gamma.add(key.mod(BigInteger.TEN).intValue());
            key = key.divide(BigInteger.TEN);
        }
        Collections.reverse(gamma);

        return Arrays.stream(gamma.toArray(new Integer[]{}))
                .mapToInt(Integer::intValue)
                .toArray();
    }

    @Override
    public void setAlphabet(String alphabet) {
        this.alphabet = alphabet;
    }

    @Override
    public String getAlphabet() {
        return alphabet;
    }

    @Override
    public Pair<BigInteger> egEncrypt(BigInteger message, BigInteger module, BigInteger g, BigInteger y, BigInteger randomSeed) {
        if (message.compareTo(module) >= 0) {
            throw new MessageIsGreaterThanModuleException();
        }

        if (g.compareTo(module) >= 0) {
            throw new GIsGreaterOrEqualsToModuleException();
        }

        if (!isPrimitive(g, module)) {
            throw new GIsNotPrimitiveException();
        }

        if (randomSeed.compareTo(module) >= 0) {
            throw new RandomSeedIsGreaterOrEqualsToModuleException();
        }

        Pair<BigInteger> result = new Pair<>();
        result.setFirst(g.modPow(randomSeed, module));
        BigInteger b = y.modPow(randomSeed, module).multiply(message).mod(module);
        result.setSecond(b);
        return result;
    }

    @Override
    public BigInteger egDecrypt(BigInteger a, BigInteger b, BigInteger module, BigInteger g, BigInteger y) {
        BigInteger x = eqPower(y, g, module);
        return egDecrypt(a, b, module, x);
    }

    @Override
    public BigInteger egDecrypt(BigInteger a, BigInteger b, BigInteger module, BigInteger randomSeed) {
        BigInteger inverseA = a.pow(randomSeed.intValue()).modInverse(module);
        return b.multiply(inverseA).mod(module);
    }

    @Override
    public boolean egCheckSignature(BigInteger message, Pair<BigInteger> signature, BigInteger module, BigInteger g, BigInteger y) {
        BigInteger leftStatement = y.modPow(signature.getFirst(), module)
                .multiply(signature.getFirst().modPow(signature.getSecond(), module))
                .mod(module);

        BigInteger rightStatement = g.modPow(message, module);
        return leftStatement.equals(rightStatement);
    }

    @Override
    public Pair<BigInteger> egSign(BigInteger message, BigInteger xSeed, BigInteger kSeed, BigInteger module, BigInteger g) {
        BigInteger r = g.modPow(kSeed, module);
        module = module.subtract(BigInteger.ONE);
        BigInteger s = message.subtract(xSeed.multiply(r)).mod(module);
        s = s.multiply(kSeed.modInverse(module)).mod(module);
        return new Pair<>(r, s);
    }
}
