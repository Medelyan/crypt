package com.gitlab.medelyan.constraints;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;
import java.math.BigInteger;

@Target({ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = MerkleHellmanKeyValidator.class)
@Documented
public @interface MerkleHellmanKey {
    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
