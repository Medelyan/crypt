package com.gitlab.medelyan.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigInteger;

public class PowerValidator implements ConstraintValidator<Power, BigInteger> {

    private int minAllowedPower;

    @Override
    public void initialize(Power constraintAnnotation) {
        minAllowedPower = constraintAnnotation.min();
    }

    @Override
    public boolean isValid(BigInteger power, ConstraintValidatorContext constraintValidatorContext) {
        return power.compareTo(new BigInteger(Integer.toString(minAllowedPower))) >= 0;
    }
}
