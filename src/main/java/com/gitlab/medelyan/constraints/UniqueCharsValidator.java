package com.gitlab.medelyan.constraints;

import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UniqueCharsValidator implements ConstraintValidator<UniqueChars, String> {

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (s == null) {
            return false;
        }

        for (int i = 0; i < s.length(); i++) {
            if (StringUtils.countMatches(s, s.charAt(i)) != 1) {
                return false;
            }
        }

        return true;
    }
}
