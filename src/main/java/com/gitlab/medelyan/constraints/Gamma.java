package com.gitlab.medelyan.constraints;

import com.gitlab.medelyan.Crypt;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = GammaValidator.class)
@Documented
public @interface Gamma {
    String message() default Crypt.EMPTY_GAMMA;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
