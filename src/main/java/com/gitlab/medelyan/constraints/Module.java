package com.gitlab.medelyan.constraints;

import com.gitlab.medelyan.Crypt;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ModuleValidator.class)
@Documented
public @interface Module {
    String message() default Crypt.BAD_VALUE;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
