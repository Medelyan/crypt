package com.gitlab.medelyan.constraints;

import com.gitlab.medelyan.Crypt;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.ANNOTATION_TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PowerValidator.class)
@Documented
public @interface Power {
    int min() default 1;

    String message() default Crypt.NOT_SUPPORTED;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
