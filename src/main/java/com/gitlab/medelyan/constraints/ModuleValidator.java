package com.gitlab.medelyan.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigInteger;

public class ModuleValidator implements ConstraintValidator<Module, BigInteger> {

    private static final int MIN_ALLOWED_MODULE = 2;

    @Override
    public boolean isValid(BigInteger module, ConstraintValidatorContext constraintValidatorContext) {
        return module.compareTo(new BigInteger(Integer.toString(MIN_ALLOWED_MODULE))) >= 0;
    }
}
