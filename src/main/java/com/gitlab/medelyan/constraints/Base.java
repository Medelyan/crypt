package com.gitlab.medelyan.constraints;

import com.gitlab.medelyan.Crypt;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.PARAMETER, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = BaseValidator.class)
@Documented
public @interface Base {
    int min() default 1;

    String message() default Crypt.NOT_SUPPORTED;

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
