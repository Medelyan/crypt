package com.gitlab.medelyan.constraints;

import com.gitlab.medelyan.Crypt;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

public class GammaValidator implements ConstraintValidator<Gamma, int[]> {

    @Override
    public boolean isValid(int[] gammaArray, ConstraintValidatorContext constraintValidatorContext) {
        if (gammaArray == null) {
            constraintValidatorContext
                    .buildConstraintViolationWithTemplate(Crypt.BAD_VALUE)
                    .addConstraintViolation();
            return false;
        }

        if (gammaArray.length == 0) {
            constraintValidatorContext
                    .buildConstraintViolationWithTemplate(Crypt.EMPTY_GAMMA)
                    .addConstraintViolation();
            return false;
        }

        if (Arrays.stream(gammaArray)
                .anyMatch(i -> i < 0)) {
            constraintValidatorContext
                    .buildConstraintViolationWithTemplate(Crypt.NEGATIVE_OFFSET)
                    .addConstraintViolation();
            return false;
        }

        return true;
    }
}
