package com.gitlab.medelyan.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigInteger;

public class SuperIncreasingSequenceValidator implements ConstraintValidator<SuperIncreasingSequence, BigInteger[]> {

    @Override
    public boolean isValid(BigInteger[] sequence, ConstraintValidatorContext constraintValidatorContext) {
        BigInteger sum = sequence[0];
        for (int i = 1; i < sequence.length; i++) {
            if( sequence[i].compareTo(sum) <= 0) {
                return false;
            }
            sum = sum.add(sequence[i]);
        }
        return true;
    }

}
