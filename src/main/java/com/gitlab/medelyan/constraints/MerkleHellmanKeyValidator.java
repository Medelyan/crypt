package com.gitlab.medelyan.constraints;

import com.gitlab.medelyan.MHPrivateKey;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigInteger;

public class MerkleHellmanKeyValidator implements ConstraintValidator<MerkleHellmanKey, MHPrivateKey> {

    @Override
    public boolean isValid(MHPrivateKey mhPrivateKey, ConstraintValidatorContext constraintValidatorContext) {
        BigInteger[] sequence = mhPrivateKey.getSequence();
        BigInteger module = mhPrivateKey.getModule();
        BigInteger coef = mhPrivateKey.getCoefficient();
        return module.compareTo(coef) > 0
                && module.compareTo(sum(sequence)) > 0
                && module.gcd(coef).compareTo(BigInteger.valueOf(1)) == 0
                && isSequenceSuperIncreasing(sequence);
    }

    private BigInteger sum(BigInteger[] sequence) {
        BigInteger sum = BigInteger.valueOf(0);
        for (BigInteger element : sequence) {
            sum = sum.add(element);
        }
        return sum;
    }

    private boolean isSequenceSuperIncreasing(BigInteger[] sequence) {
        BigInteger sum = sequence[0];
        for (int i = 1; i < sequence.length; i++) {
            if( sequence[i].compareTo(sum) <= 0) {
                return false;
            }
            sum = sum.add(sequence[i]);
        }
        return true;
    }
}
