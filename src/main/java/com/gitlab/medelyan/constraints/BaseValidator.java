package com.gitlab.medelyan.constraints;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigInteger;

public class BaseValidator implements ConstraintValidator<Base, BigInteger> {

    private int minAllowedBase;

    @Override
    public void initialize(Base constraintAnnotation) {
        minAllowedBase = constraintAnnotation.min();
    }

    @Override
    public boolean isValid(BigInteger base, ConstraintValidatorContext constraintValidatorContext) {
        return base.compareTo(new BigInteger(Integer.toString(minAllowedBase))) >= 0;
    }
}
