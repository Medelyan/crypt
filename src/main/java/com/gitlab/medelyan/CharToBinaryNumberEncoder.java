package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.UnknownCharException;

public class CharToBinaryNumberEncoder implements EncodingCharStrategy {
    @Override
    public String encode(char ch, String alphabet) {
        int number = alphabet.indexOf(ch);
        if (number == -1) {
            throw new UnknownCharException("Char is not in the alphabet.");
        }
        return Integer.toBinaryString(number + 1);
    }

    /**
     * Decodes char that's been encoded with {@link CharToBinaryNumberEncoder#encode(char, String)} method
     *
     * @param encodedChar char encoded as a sequence of 0s and 1s
     * @param alphabet    alphabet to use for decoding
     * @return decoded char
     */
    @Override
    public char decode(String encodedChar, String alphabet) {
        int letterIndex = Integer.parseInt(encodedChar, 2);
        return alphabet.charAt(letterIndex - 1);
    }
}
