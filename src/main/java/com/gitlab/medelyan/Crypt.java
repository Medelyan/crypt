package com.gitlab.medelyan;

import com.gitlab.medelyan.constraints.*;
import com.gitlab.medelyan.exceptions.EquationHasNoSolutionsException;
import com.gitlab.medelyan.exceptions.UnsuccessfulBruteforceException;
import com.gitlab.medelyan.exceptions.UnsuccessfulCommonModuleAttackException;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;

/**
 * This interface provides encryption, decryption and other service functions
 */
public interface Crypt {
    String NOT_SUPPORTED = "value is unsupported";
    String BAD_VALUE = "bad value";
    String EMPTY_GAMMA = "empty gamma";
    String NEGATIVE_OFFSET = "negative offsets are not supported";
    String EMPTY_ALPHABET = "empty alphabet";
    String REPEATING_SYMBOLS = "alphabet contains repeating symbols";
    String NOT_SUPER_INCREASING_SEQUENCE = "super increasing sequence required";
    String BAD_MH_PRIVATE_KEY = "bad private key for Merkle-Hellman crypto-system";

    static Crypt build() {
        return (Crypt) MethodArgumentsValidatingProxy.newInstance(new CryptImpl());
    }

    /**
     * A convenient and meaningfull shortcut for gamma array
     *
     * @param offsets offsets
     * @return array of given offsets
     */
    int[] gamma(@Gamma int... offsets);

    /**
     * @return current set alphabet for encryption/decryption
     */
    String getAlphabet();

    /**
     * Sets alphabet to be used for encryption/decryption
     *
     * @param alphabet string, containing all characters of alphabet in desired ascending order
     */
    void setAlphabet(@NotNull(message = BAD_VALUE)
                     @NotEmpty(message = EMPTY_ALPHABET)
                     @UniqueChars(message = REPEATING_SYMBOLS) String alphabet);

    /**
     * Encrypt text with gamma. Input text parameter must match the alphabet.
     * If it doesn't, then {@link com.gitlab.medelyan.exceptions.UnknownCharException UnknownCharException}
     * will be thrown.
     *
     * @return encrypted text
     */
    String encrypt(@NotNull(message = BAD_VALUE) String text,
                   @Gamma int[] gamma);

    /**
     * Decrypts encrypted text with given gamma. Input text parameter must match the alphabet.
     * If it doesn't, then {@link com.gitlab.medelyan.exceptions.UnknownCharException UnknownCharException}
     * will be thrown.
     *
     * @return decrypted text
     */
    String decrypt(@NotNull(message = BAD_VALUE) String encrypted,
                   @Gamma int[] gamma);

    /**
     * Extracts gamma array based on a number
     *
     * @param key the number
     * @return extracted gamma
     */
    int[] extractGamma(BigInteger key);

    /**
     * Calculates key in Diffie-Hellman`s algorithm
     *
     * @param base   q in DH algorithm
     * @param alpha  Alice`s alpha in DH algorithm
     * @param beta   Bob`s beta in DH algorithm
     * @param module m in DH algorithm
     * @return k in DH algorithm
     */
    BigInteger dhKey(@Base(min = 2, message = BAD_VALUE) BigInteger base,
                     @Power(message = BAD_VALUE) BigInteger alpha,
                     @Power(message = BAD_VALUE) BigInteger beta,
                     @Module BigInteger module);

    /**
     * Encrypts given text accordingly to Diffie-Hellman algorithm
     *
     * @param text   text to encrypt
     * @param base   q in DH algorithm
     * @param module m in DH algorithm
     * @param alpha  alpha in DH algorithm
     * @param beta   beta in DH algorithm
     * @return encrypted text
     */
    String dhEncrypt(@NotNull(message = BAD_VALUE) String text,
                     @Base BigInteger base,
                     @Module BigInteger module,
                     @Power(message = BAD_VALUE) BigInteger alpha,
                     @Power(message = BAD_VALUE) BigInteger beta);

    /**
     * Decrypts message encrypted with Diffie-Hellman algorithm
     *
     * @param encrypted text to decrypt
     * @param base      q in DH algorithm
     * @param alpha     Alice`s alpha in DH algorithm
     * @param beta      Bob`s beta in DH algorithm
     * @param module    m in DH algorithm
     * @return decrypted message
     */
    String dhDecrypt(@NotNull(message = BAD_VALUE) String encrypted,
                     @Base(min = 2, message = BAD_VALUE) BigInteger base,
                     @Power(message = BAD_VALUE) BigInteger alpha,
                     @Power(message = BAD_VALUE) BigInteger beta,
                     @Module BigInteger module);

    /**
     * Decrypts message encrypted with Diffie Hellman algorithm
     *
     * @param encrypted text to decrypt
     * @param key       k in DH algorithm
     * @return decrypted message
     */
    String dhDecrypt(@NotNull(message = BAD_VALUE) String encrypted,
                     @Min(value = 1, message = BAD_VALUE) BigInteger key);

    /**
     * Tries to brute-force encrypted text with given parameters assuming that it has been
     * encrypted with Diffie Hellman's algorithm
     *
     * @param encrypted encrypted text to brute-force
     * @param base      base (q in DH algorithm)
     * @param module    module (m in DH algorithm)
     * @param x         x (result of base ^ alpha, where alpha is part of secret key)
     * @param y         y (result of base ^ beta, where beta is a part of secret key)
     * @return decrypted text
     */
    String dhBruteforce(@NotNull(message = BAD_VALUE) String encrypted,
                        @Base BigInteger base,
                        @Module BigInteger module,
                        @Min(value = 1, message = BAD_VALUE) BigInteger x,
                        @Min(value = 1, message = BAD_VALUE) BigInteger y) throws UnsuccessfulBruteforceException;

    /**
     * Finds the least possible power in equation (if possible):
     * x = (base ^ power) % module.
     * If equation does not have a solution, then
     * {@link EquationHasNoSolutionsException EquationHasNoSolutionsException}
     * will be thrown.
     *
     * @return power if possible
     */
    BigInteger eqPower(@Min(value = 0, message = NOT_SUPPORTED) BigInteger x,
                       @Base(min = 2, message = BAD_VALUE) BigInteger base,
                       @Module BigInteger module);

    /**
     * Finds order of groupElement by given module. Infinite orders are
     * not supported, so {@link com.gitlab.medelyan.exceptions.InfiniteOrderException InfiniteOrderException}
     * will be thrown in this case.
     *
     * @param groupElement element of Z-group
     * @param module       module of Z-group
     * @return order of groupElement
     */
    BigInteger orderOf(@Min(value = 2, message = BAD_VALUE) BigInteger groupElement,
                       @Module BigInteger module);

    /**
     * Check whether given groupElement is primitive in Z-field
     * with given module.
     *
     * @param groupElement element in Z-field
     * @param module       module of Z-field
     * @return true if groupElement is primitive, false otherwise
     */
    boolean isPrimitive(@Min(value = 0, message = BAD_VALUE) BigInteger groupElement,
                        @Module BigInteger module);

    /**
     * Finds all of the primitive elements of the field Z by given module
     *
     * @param module module of the field
     * @return array of primitive elements
     */
    BigInteger[] primitivesBy(@Module BigInteger module);

    /**
     * Calculates Euler function for given value, i. e.
     * finds number of positive integers up to a given integer
     * that are relatively prime to {@code n}
     *
     * @param value value to find value of euler function for
     * @return number of relatively prime integers to the given one
     * and not greater than it
     */
    BigInteger euler(@Min(value = 1, message = BAD_VALUE) BigInteger value);

    /**
     * Tries to solve an equation:
     * A * x = B (mod module)
     * <p>
     * A has to be prime to module.
     * <p>
     * Module has to be relatively small. Otherwise it will take
     * too much time to calculate euler of module. In this case
     * use {@link Crypt#eqSolve(BigInteger, BigInteger, BigInteger, BigInteger)} instead.
     *
     * @param a      A, has to be prime to module
     * @param b      B
     * @param module module
     * @return the least possible x if any
     */
    BigInteger eqSolve(@Min(value = 1, message = NOT_SUPPORTED) BigInteger a,
                       @Min(value = 1, message = NOT_SUPPORTED) BigInteger b,
                       @Module BigInteger module);

    /**
     * Tries to solve an equation:
     * A * x = B (mod module)
     * <p>
     * A has to be prime to module
     *
     * @param a             A, has to be prime to module
     * @param b             B
     * @param module        module
     * @param eulerOfModule result of the euler function of the module
     * @return the least possible x if any
     */
    BigInteger eqSolve(@Min(value = 1, message = NOT_SUPPORTED) BigInteger a,
                       @Min(value = 1, message = NOT_SUPPORTED) BigInteger b,
                       @Module BigInteger module,
                       @Min(value = 1, message = BAD_VALUE) BigInteger eulerOfModule);

    /**
     * TODO: [SM] Provide a more comprehensive commentary
     * Encrypts text
     *
     * @param publicKey public key for Merkle-Hellman algorithm
     * @param encoder   encoding strategy to be used for char encoding
     * @return array of encrypted letters
     */
    BigInteger[] mhEncrypt(@NotNull(message = BAD_VALUE) String text, BigInteger[] publicKey, EncodingCharStrategy encoder);

    /**
     * Encrypts text. Uses binary view of letter number in alphabet as a char encoding strategy.
     *
     * @param publicKey public key for Merkle-Hellman algorithm
     * @return array of encrypted letters
     */
    BigInteger[] mhEncrypt(@NotNull(message = BAD_VALUE) String text, BigInteger[] publicKey);

    /**
     * Calculates public key for Merkle-Hellman crypto-system
     *
     * @param privateKey private key
     * @return public key
     */
    BigInteger[] mhPublicKey(@MerkleHellmanKey(message = BAD_MH_PRIVATE_KEY) MHPrivateKey privateKey);

    /**
     * Decrypts message encrypted with Merkle-Hellman algorithm
     *
     * @param encrypted array of numbers which are encrypted letters
     * @param key       private key for Merkle-Hellman crypto-system
     * @param encoder   strategy to be used for char decoding
     * @return decrypted text
     */
    String mhDecrypt(BigInteger[] encrypted,
                     @MerkleHellmanKey(message = BAD_MH_PRIVATE_KEY) MHPrivateKey key,
                     EncodingCharStrategy encoder);

    /**
     * Decrypts message encrypted with Merkle-Hellman algorithm. Uses binary view of letter number in alphabet as a char encoding strategy.
     *
     * @param encrypted array of numbers which are encrypted letters
     * @param key       private key for Merkle-Hellman crypto-system
     * @return decrypted text
     */
    String mhDecrypt(BigInteger[] encrypted,
                     @MerkleHellmanKey(message = BAD_MH_PRIVATE_KEY) MHPrivateKey key);

    Pair<BigInteger> egEncrypt(@Min(value = 1, message = Crypt.BAD_VALUE) BigInteger message,
                               @Module BigInteger module,
                               @Min(value = 2, message = Crypt.BAD_VALUE) BigInteger g,
                               @Min(value = 0, message = BAD_VALUE) BigInteger y,
                               @Min(value = 1, message = BAD_VALUE) BigInteger randomSeed);

    /**
     * Decrypts a message with attempt to calculate random seed x
     * in ElGamal encryption system
     *
     * @param a      first number of response from Alice
     * @param b      second number of response from Alice
     * @param module module of the system
     * @param g      primitive element based on module
     * @param y      last number in calculated public key
     * @return decrypted message
     */
    BigInteger egDecrypt(BigInteger a, BigInteger b, BigInteger module, BigInteger g, BigInteger y);

    /**
     * Decrypts a message according to ElGamal encryption system
     *
     * @param a          first number of response from Alice
     * @param b          second number of response from Alice
     * @param module     module of the system
     * @param randomSeed random seed X of the system
     * @return decrypted message
     */
    BigInteger egDecrypt(BigInteger a, BigInteger b, BigInteger module, BigInteger randomSeed);

    /**
     * Checks if the given signature is valid for the given message and public key - all based
     * on the ElGamal encryption system
     *
     * @param message   message that was signed
     * @param signature signature of the message
     * @param module    module of the ElGamal encryption system
     * @param g         primitive element in the system
     * @param y         last number in calculated public key
     * @return true if signature is valid, false otherwise
     */
    boolean egCheckSignature(BigInteger message, Pair<BigInteger> signature, BigInteger module, BigInteger g, BigInteger y);

    /**
     * Creates signature for the given message according to ElGamal encryption system
     *
     * @param message message to sign
     * @param xSeed   x parameter
     * @param kSeed   k parameter (session-generated)
     * @param module  module of the system
     * @param g       primitive in the system
     * @return signature for the message
     */
    Pair<BigInteger> egSign(BigInteger message, BigInteger xSeed, BigInteger kSeed, BigInteger module, BigInteger g);

    /**
     * Decrypts message according to RSA encryption algorithm
     *
     * @param message encrypted message
     * @param r       part of public key
     * @param b       part of public key
     * @return decrypted message
     */
    BigInteger rsaDecrypt(BigInteger message, BigInteger r, BigInteger b);

    /**
     * Encrypts given message according to RSA encryption algorithm
     *
     * @param message message to encrypt
     * @param r       module of the recipient to use
     * @param b       public key of the recipient to use
     * @return encrypted message
     */
    BigInteger rsaEncrypt(BigInteger message, BigInteger r, BigInteger b);

    /**
     * Attack on the base of common module. Finds secret message m.
     *
     * @param commonModule common module
     * @param a            part of public key of participant A
     * @param b            part of public key of participant B
     * @param m1           the first secret message (from A to B)
     * @param m2           the second secret message (from B to someone else)
     * @return original secret message m
     */
    BigInteger rsaCommonModuleAttack(BigInteger commonModule, BigInteger a, BigInteger b, BigInteger m1, BigInteger m2) throws UnsuccessfulCommonModuleAttackException;

    /**
     * Checks that sign is right.
     *
     * @param p p (primary number)
     * @param q q (primary number)
     * @param g g
     * @param y y (part of public key)
     * @param r r (part of public key)
     * @param e e (recipient's randomSeed)
     * @param s signature
     * @return true if sign belongs to the right person else false
     */
    boolean schnorrCheckSignature(BigInteger p, BigInteger q, BigInteger g, BigInteger y, BigInteger r, BigInteger e, BigInteger s);

    /**
     * Creates signature according Schnorr algorythm
     *
     * @param q          primary number
     * @param k          private key
     * @param a          issuer's secret key
     * @param randomSeed randomSeed from the recipient to use
     * @return signature (y, r)
     */
    BigInteger schnorrSign(BigInteger q, BigInteger a, BigInteger k, BigInteger randomSeed);
}
