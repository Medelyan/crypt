package com.gitlab.medelyan;

/**
 * Strategy of one character encoding
 */
public interface EncodingCharStrategy {
    // TODO: [SM] We'd better refactor the method to take/return Object
    String encode(char ch, String alphabet);
    char decode(String letterBitMask, String alphabet);
}
