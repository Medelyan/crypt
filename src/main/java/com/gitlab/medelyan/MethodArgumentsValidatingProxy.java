package com.gitlab.medelyan;

import com.gitlab.medelyan.exceptions.ConstraintViolationException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import javax.validation.executable.ExecutableValidator;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Set;

public class MethodArgumentsValidatingProxy<T> implements InvocationHandler {

    private static final ExecutableValidator validator;

    static {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator().forExecutables();
    }

    private T target;

    private MethodArgumentsValidatingProxy(T target) {
        this.target = target;
    }

    @SuppressWarnings("unchecked")
    static Object newInstance(Crypt target) {
        InvocationHandler handler = new MethodArgumentsValidatingProxy<>(target);
        return Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(), handler);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (args != null) {
            Set<ConstraintViolation<T>> violations = validator.validateParameters(target, method, args);
            validate(violations);
        }

        return invoke(method, args);
    }

    private void validate(Set<ConstraintViolation<T>> violations) {
        if (violations.size() > 0) {
            String message = violations.stream()
                    .map(ConstraintViolation::getMessage)
                    .reduce((s1, s2) -> s1 + "\n" + s2)
                    .get();
            throw new ConstraintViolationException(message);
        }
    }

    private Object invoke(Method method, Object[] args) throws Throwable {
        try {
            return method.invoke(target, args);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }
}
