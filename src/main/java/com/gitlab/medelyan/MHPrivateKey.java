package com.gitlab.medelyan;

import com.gitlab.medelyan.constraints.MerkleHellmanKey;
import com.gitlab.medelyan.constraints.SuperIncreasingSequence;

import java.math.BigInteger;

/**
 * Private key for Merkle-Hellman crypto-system
 */
public class MHPrivateKey {

    private BigInteger[] sequence;

    /**
     * Module used to calculate the sequence for encryption
     */
    private BigInteger module;

    /**
     * Coefficient by which elements of super-increasing sequence were multiplied while calculating sequence for encryption
     */
    private BigInteger coefficient;

    public MHPrivateKey(@SuperIncreasingSequence(message = Crypt.NOT_SUPER_INCREASING_SEQUENCE) BigInteger[] sequence,
                        BigInteger module, BigInteger coefficient) {
        this.sequence = sequence;
        this.module = module;
        this.coefficient = coefficient;
    }

    public BigInteger[] getSequence() {
        return sequence;
    }

    public BigInteger getModule() {
        return module;
    }

    public BigInteger getCoefficient() {
        return coefficient;
    }
}
