[![](https://jitpack.io/v/com.gitlab.Medelyan/crypt.svg)](https://jitpack.io/#com.gitlab.Medelyan/crypt)
[&#91;Examples&#93;](https://gitlab.com/Medelyan/crypt/blob/master/EXAMPLES_RU.md)
[&#91;Changelog&#93;](https://gitlab.com/Medelyan/crypt/blob/master/CHANGELOG.md)
# Crypt
## Содержание

* [Сводка](#сводка)
* [Использование](#использование)
    * [Добавьте зависимость](#добавьте-зависимость)
    * [Применяйте](#применяйте)
* [Участие в проекте](#участие-в-проекте)

## Сводка

Этот проект содержит полезные функции для шифрования/дешифрования текста и был создан в
учебных целях.

## Использование

### Добавьте зависимость

Перейдите на [страницу проекта на JitPack](https://jitpack.io/#com.gitlab.Medelyan/crypt). Следуя инструкциям,
там вы сможете выбрать нужную вам версию.

> Замечание: Если вы хотите также иметь javadoc-документацию, вы можете сделать это прямо в вашей IDE.
В случае IntelliJ IDEA: ПКМ на корневой папке проекта, затем выберите `Maven -> Download documentation` (или аналогичный
пункт для Gradle и других сборщиков/менеджеров).  
FIXME: SM: У меня не работает :(

### Применяйте 

Сначала вам нужно "построить крипту". 
```java
Crypt crypt = Crypt.build();
```

После этого вы можете её непосредственно использовать, например:
```java
// для шифрования/дешифрования гаммированием сначала нужно указать,
// какой алфавит будет использоваться
// (достаточно задать один раз для одного экземпляра)
crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
int[] gamma = new int[]{30, 14, 4, 20, 4};
String decrypted = crypt.decrypt("сымцйоямжйр", gamma);
decrypted.equals("университет"); // -> true
```

Взгляните также на [Примеры](https://gitlab.com/Medelyan/crypt/blob/master/EXAMPLES_RU.md) для наглядной демонстрации использования библиотеки в решении
задач.

## Участие в проекте

Для инструкций прочтите [Contribution guide](https://gitlab.com/Medelyan/crypt/blob/master/CONTRIBUTING.md).