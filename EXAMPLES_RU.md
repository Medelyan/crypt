# Примеры задач

```java
Crypt crypt = Crypt.build();
crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);

// 1. Используя гамму γ = (5, 17, 2, 31, 4, 8), зашифруйте слово «ТРАЕКТОРИЯ».
int[] gamma = crypt.gamma(5, 17, 2, 31, 4, 8);
String text = "траектория";
String encrypted = crypt.encrypt(text, gamma);
System.out.println(text + " -> " + encrypted);
// Output: траектория -> чбвдоъубкю

// 2. На основе шифротекста «ЮФВЭТЧГВ» и гаммы γ = (30,  3,  16,  24,  2,  9,  27)
// определите зашифрованное слово.
encrypted = "юфвэтчгв";
gamma = crypt.gamma(30, 3, 16, 24, 2, 9, 27);
String decrypted = crypt.decrypt(encrypted, gamma);
System.out.println(encrypted + " -> " + decrypted);
// Output: юфвэтчгв -> астероид

// 3. Алиса и Боб обмениваются секретными сообщениями, используя для шифрования гаммирование.
// Для формирования гаммы они применяют алгоритм Диффи–Хеллмана со следующими параметрами
// открытого ключа:
//
// m = 41903,
// q = 10109,
// x = 25789,
// y = 32011.
//
//Цифры общего секретного ключа k они используют в качестве базы для формирования гаммы. Алиса отправила Бобу шифротекст «ФНСЕУПУИЕИТ»
//Расшифруйте это сообщение.
BigInteger base = BigInteger.valueOf(10109);
BigInteger module = BigInteger.valueOf(41903);
BigInteger x = BigInteger.valueOf(25789);
BigInteger y = BigInteger.valueOf(32011);
encrypted = "фнсеупуиеит";
decrypted = crypt.dhBruteforce(encrypted, base, module, x, y);
System.out.println(encrypted + " -> " + decrypted);
// Output: фнсеупуиеит -> тираннозавр

// 4. Найти все примитивные элементы поля Z с модулем 41.
BigInteger module = BigInteger.valueOf(41);
BigInteger[] primitiveElements = crypt.primitivesBy(module);
System.out.println(Arrays.toString(primitiveElements);
// Output: [6, 7, 11, 12, 13, 15, 17, 19, 22, 24, 26, 28, 29, 30, 34, 35 ]

// 5. Алиса и Боб обмениваются сообщениями с помощью гаммирования.
// Для формирования гаммы γ используется алгоритм Диффи-Хеллмана
// m = 41903
// q = 10109
// x = 11108
// y = 35218
//
// Однако была проведена атака Man-in-the-Middle со следующим ключом:
// α = 29026
// β = 33872
// Алиса назначила Бобу встречу в следующем городе:
// ннллхсшфв
// Но Боб получил следующий город от Man-in-the-Middle:
// фоувйьшу
//
// Определить оба города.
//
BigInteger module = BigInteger.valueOf(41903);
BigInteger base = BigInteger.valueOf(10109);
BigInteger x = BigInteger.valueOf(11108);
BigInteger y = BigInteger.valueOf(35218);

BigInteger fakeAlpha = BigInteger.valueOf(29026);
BigInteger fakeBeta = BigInteger.valueOf(33872);

String enc1 = "ннллхсшфв";
String enc2 = "фоувйьшу";

BigInteger fakeY = base.modPow(fakeBeta, module);
String town1 = crypt.dhBruteforce(enc1, base, module, x, fakeY);

BigInteger fakeX = base.modPow(fakeAlpha, module);
String town2 = crypt.dhBruteforce(enc2, base, module, fakeX, y);

System.out.println("town1 - " + town1);
System.out.println("town2 - " + town2);
// Output:
// town1 - ливерпуль
// town2 - мельбурн

// Алиса и Боб используют систему без передачи ключей. Каждая буква 
// шифруется её номером в алфавите к которому добавляют число 9.
// А - 10, Б - 11, ...
// Буквы Ё нет.
// Известно число p = 62 923 926 601
// Боб выбрал B = 678 433
// Известно 2 шифротекста:
// m1 = 2 146 916 375
// m2 = 34 979 060 413
// Найти исходное зашифрованное слово
BigInteger module = BigInteger.valueOf(62_923_926_601L);
BigInteger b = BigInteger.valueOf(678_433L);
BigInteger m3 = BigInteger.valueOf(34_979_060_413L);
BigInteger eulerOfModule = module.subtract(BigInteger.ONE);

BigInteger beta = crypt.eqSolve(b, BigInteger.ONE, eulerOfModule);
BigInteger m = m3.modPow(beta, module);
System.out.println(decrypt(m.toString()));
// Output: океан

private static String decrypt(String m) {
    if(m.length() % 2 != 0) {
        return "ERROR: length is not even";
    }

    StringBuilder result = new StringBuilder();
    String current = m;
    while(current.length() > 0) {
        String value = current.substring(0, 2);
        if(current.length() > 2) {
            current = current.substring(2);
        } else {
            current = "";
        }
        int index = Integer.parseInt(value) - 10;
        result.append(Alphabets.RUSSIAN_LOWERCASE.charAt(index));
    }
    return result.toString();
}

// 6. Для шифрования используется система Меркля-Хеллмана. 
// Используется русский алфавит без ё. Буквы кодируются номером 
// буквы алфавита в двоичной системе: а <-> 000001, ..., я <-> 100000.
// Выбрана супервозрастающая последовательность w=(1, 2, 4, 9, 18, 35), 
// q = 80, r = 29.
// Расшифровать сообщение x = (55, 97, 21, 78, 100, 155)
BigInteger[] encrypted = new BigInteger[] {
     BigInteger.valueOf(55),
     BigInteger.valueOf(97),
     BigInteger.valueOf(21),
     BigInteger.valueOf(79),
     BigInteger.valueOf(100),
     BigInteger.valueOf(155),
};
BigInteger[] seq = new BigInteger[] {
       BigInteger.valueOf(1),
       BigInteger.valueOf(2),
       BigInteger.valueOf(4),
       BigInteger.valueOf(9),
       BigInteger.valueOf(18),
       BigInteger.valueOf(35),
};
BigInteger module = BigInteger.valueOf(80);
BigInteger coef = BigInteger.valueOf(29);
MHPrivateKey key = new MHPrivateKey(seq, module, coef);
String decrypted = crypt.mhDecrypt(encrypted, key);
System.out.println(decrypted);
// Output: август

// 7. A и B решили обменяться секретными сообщениями с использованием
// процедуры гаммирования. Для формирования γ они воспользовались
// схемой шифрования Эль-Гамаля.
// Боб отправил Алисе (p, g, y) = (54709, 23113, 25058),
// а Алиса ответила (a, b) = (926, 16005).
// Цифры исходного сообщения Q используются в качестве базы для γ.
// Боб отправил Алисе: "фсйощгмпа". Расшифровать сообщение.
BigInteger module = BigInteger.valueOf(54709);
BigInteger g = BigInteger.valueOf(23113);
BigInteger y = BigInteger.valueOf(25058);
BigInteger a = BigInteger.valueOf(926);
BigInteger b = BigInteger.valueOf(16005);

BigInteger gammaBase = crypt.egDecrypt(a, b, module, g, y);
int[] gamma = crypt.extractGamma(gammaBase);
String encrypted = "фсйощгмпа";

crypt.setAlphabet(Alphabets.RUSSIAN_LOWERCASE);
String decrypted = crypt.decrypt(encrypted, gamma);
System.out.println(decrypted);
// Output: спектакль
```